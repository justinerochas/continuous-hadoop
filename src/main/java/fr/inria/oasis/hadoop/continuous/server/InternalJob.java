package fr.inria.oasis.hadoop.continuous.server;

import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.security.TokenCache;
import org.apache.hadoop.security.UserGroupInformation;

class InternalJob implements Cloneable {
	private JobID jobID;
	private Path submitDir;
	private JobConf jobConf;

	private String input;
	private String output;
	private String originalOutput;

	private long inputSize;
	private String time;

	private Path localXML;
	private Path localJar;
	private String newInputFilePath;
	private UserGroupInformation ugi;

	// For Managing Dependency Graph;
	private List<InternalJob> previousJobs = new LinkedList<InternalJob>();
	private List<InternalJob> nextJobs = new LinkedList<InternalJob>();
	private int visitedOfPreviousJobs;

	public InternalJob(JobID jobID, Path submitDir, JobConf jobConf,  String input, String output,
			Path localXML, Path localJar,UserGroupInformation ugi){
		this.jobID = jobID;
		this.submitDir = submitDir;
		this.jobConf = jobConf;

		this.input = input;
		this.output = output;
		this.originalOutput = output;

		this.inputSize = 0;
		this.time = null;

		this.localXML = localXML;
		this.localJar = localJar;

		// For Managing Dependency Graph;
		this.visitedOfPreviousJobs = 0;
		this.ugi = ugi;
	}

	public UserGroupInformation getUgi()
	{
		return this.ugi;
	}

	public JobID getJobID(){
		return this.jobID;

	}

	public Path getSubmitDir(){
		return this.submitDir;
	}

	public void setJobConf(JobConf jobConf){
		this.jobConf = jobConf;
	}

	public JobConf getJobConf(){
		return this.jobConf;
	}

	public String getInputFolder(){
		return this.input;
	}

	public String getOutputFolder(){
		return this.output;
	}

	public void setInputSize(long inputSize){
		this.inputSize = inputSize;
	}

	public long getInputSize(){
		return this.inputSize;
	}

	public void setContinuousJobId(JobID jobId){
		this.jobID = jobId;
	}

	public void setTime(String d){
		this.time = d;
	}

	public String getTime(){
		return this.time;
	}

	public Path getLocalXML(){
		return this.localXML;
	}

	public Path getLocalJar(){
		return this.localJar;
	}

	public List<InternalJob> getPreviousJobs(){
		return this.previousJobs;
	}

	public List<InternalJob> getNextJobs(){
		return this.nextJobs;
	}

	public void setOutput(String output){
		this.output = output;
	}

	public void increaseVisitedValue(){
		this.visitedOfPreviousJobs++;
	}

	public void resetVisitedValue(){
		this.visitedOfPreviousJobs = 0;
	}

	public int getVisitedValue(){
		return this.visitedOfPreviousJobs;
	}

	public String getOrinalOutput(){
		return this.originalOutput;
	}

	public void setNewInputFilePath(String newInputFilePath){
		this.newInputFilePath = newInputFilePath;
	}

	public String getNewInputFilePath(){
		return this.newInputFilePath;
	}

	public void resetNewInputFilePath(){
		this.newInputFilePath = null;
	}
}