package fr.inria.oasis.hadoop.continuous.config;

import org.apache.hadoop.io.Text;

/**
 * Provides a format to read Text keys and Text values from bytes in a carry file.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 */
public class TextTextCarryFormat implements CarryFormat<Text, Text>{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Text readKey(byte[] keyBytes) {

		Text key = new Text() ;
		key.set(keyBytes) ;

		return key ;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Text readValue(byte[] valueBytes) {

		Text value = new Text() ;
		value.set(valueBytes) ;

		return value ;
	}

}
