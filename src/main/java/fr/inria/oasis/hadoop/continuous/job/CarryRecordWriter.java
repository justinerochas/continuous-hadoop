package fr.inria.oasis.hadoop.continuous.job;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.mapred.TextOutputFormat;

/**
 * You must use this class to write on the carry file
 * @author Clement Agarini, Justine Rochas, Anthony Damiano, Ludwig Poggi
 *
 * @param <K> The Hadoop type for the carry key
 * @param <V> The Hadoop type for the carry value
 */
public class CarryRecordWriter<K, V> extends TextOutputFormat<K, V> {

	private String path;
	private String separator;
	private FSDataOutputStream carryHdfsStream ;

	/**
	 *
	 * @param path for the carry file on HDFS
	 * @param separtator the separator between key and value
	 */
	public CarryRecordWriter(String path, String separtator, JobConf conf) {

		this.path = path;
		this.separator = separtator;

		try {

			FileSystem fs = FileSystem.get(conf) ;
			this.carryHdfsStream = fs.create(new Path(this.path)) ;
		}

		catch (IOException e) {

			e.printStackTrace();
		}
	}



	public RecordWriter<K, V> getWriter() {
		RecordWriter<K, V> carryOutput = new LineRecordWriter<K, V>(this.carryHdfsStream, this.separator);
		return carryOutput ;
	}


	public void close() {
		try {

			if (this.carryHdfsStream != null) {

				this.carryHdfsStream.close() ;
			}
		}

		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
