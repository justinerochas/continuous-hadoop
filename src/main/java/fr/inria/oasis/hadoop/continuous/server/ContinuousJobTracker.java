/**
 * Continuous Job Management (CJM)
 *
 * This is the implementation for the CJM server.
 * By default, CJM server will run on port 9021 in the same host of JobTracker.
 * If we want to define which host/port will launch CJM, we need to define it in the XML file
 * with this property "mapred.continuous.manager.control.port"
 *
 * @author VU Trong Tuan
 */
package fr.inria.oasis.hadoop.continuous.server;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.RPC.Server;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobTracker;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.JobStatus;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.net.NetUtils;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.util.StringUtils;

import fr.inria.oasis.hadoop.continuous.callback.SubmissionCallBackProtocol;
import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;
import fr.inria.oasis.hadoop.continuous.util.ContinuousUtil;
import fr.inria.oasis.hadoop.continuous.util.IntrospectionTool;


public class ContinuousJobTracker implements ContinuousJobTrackerProtocol {

	public final long  versionID = 0L;

	public static final String DIR_FORMATS = "mapreduce.input.multipleinputs.dir.formats";

	public static final String DIR_MAPPERS = "mapreduce.input.multipleinputs.dir.mappers";

	public static final String INPUT_DIR = "mapred.input.dir";

	public static final String OUTPUT_DIR = "mapred.output.dir";

	public static final Log LOG = LogFactory.getLog(ContinuousJobTracker.class.getName());

	private final SimpleDateFormat modifFmt = new SimpleDateFormat(ContinuousJobConfig.DATE_FORMAT);

	private static final String SUBDIR = "Continuous_Job_Manager";

	private final String HDFS_DIR = "fs.default.name";

	private JobTracker jobTracker;

	private Server server;

	private FileSystem fs = null;

	Map<Path, Set<InternalJob>> continuousJobManager = null;

	// This one is for keeping all the jobs which have been submitted to CJM
	Map<String, InternalJob> mapOfAllContinuousJob = null;

	ArrayList<String> waitingForSubmit  = new ArrayList<String>();

	private Thread thread;

	private LocalFileSystem localFileSystem = null;
	private String inputEvent = null;

	public ContinuousJobTracker(JobTracker jobTracker) throws IOException {
		this.continuousJobManager = new HashMap<Path, Set<InternalJob>>();
		this.mapOfAllContinuousJob = new HashMap<String, InternalJob>();

		this.jobTracker = jobTracker;
		this.localFileSystem = new LocalFileSystem();
		Job j = new Job();

	}

	public ContinuousJobTracker() throws IOException {
		this.continuousJobManager = new HashMap<Path, Set<InternalJob>>();
		this.mapOfAllContinuousJob = new HashMap<String, InternalJob>();

		this.localFileSystem = new LocalFileSystem();
		this.fs = FileSystem.get(new Configuration());
	}


	public void open() throws IOException {

		Configuration conf = new Configuration();
		InetSocketAddress serverAddress = ContinuousUtil.getContinuousJobTrackerServerAddress(conf);
		this.server = RPC.getServer(this, serverAddress.getHostName(),
				serverAddress.getPort(), conf);

		server.start();
		System.out.println("	TUAN, OASIS_INRIA: " + ContinuousJobTracker.class
				+ " Server is STARTED ");
	}

	public void close() throws IOException {
		this.server.stop();
		System.out.println("	TUAN, OASIS_INRIA: " + ContinuousJobTracker.class
				+ " Server is STOPPED ");
	}

	public long getProtocolVersion() throws IOException, InterruptedException {
		return this.versionID;
	}

	/**
	 * This is the RPC method defined in the ContinuousJobProtocol.java If the
	 * job is continuous, it will be submitted to the JobTracker as the normal
	 * one. Furthermore, it is also submitted to the CJM (Continuous Job
	 * Management). CJM will manage the continuous job; and the job will be
	 * triggered again when its input data increases.
	 *
	 * @author VU Trong Tuan
	 */

	public void addContinuousJob(JobConf jobConf, JobID jobID,
			String submitDir, Credentials ts) throws IOException,
			InterruptedException {

		System.out.println("addContinuousJob is STARTED");

		/*---------------Copy job.xml from HDFS submitDir to local-----------------*/
		String remote = submitDir.toString() + "/job.xml";
		Path jobXmlRemoteFile = new Path(remote);

		FileSystem fs = new Path(submitDir).getFileSystem(new Configuration());
		if (this.fs == null)
			this.fs = fs;

		Path jobXmlLocalFile = new JobConf().getLocalPath(ContinuousJobTracker.SUBDIR + "/" + jobID + "/job.xml");

		fs.copyToLocalFile(jobXmlRemoteFile, jobXmlLocalFile);

		/*---------------Copy job.jar from HDFS submitDir to local-----------------*/
		// Due to we need to remember the job.jar of the submitted job for
		// resubmitting this job to the jobtracker,
		// so the CJM needs to remember job.jar to its local machine
		Path jobJarRemoteFile = new Path(submitDir.toString() + "/job.jar");
		Path jobJarLocalFile = new JobConf().getLocalPath(ContinuousJobTracker.SUBDIR + "/" + jobID + "/job.jar");

		fs.copyToLocalFile(jobJarRemoteFile, jobJarLocalFile);

		/*---------------Creating the ContinuousJob-----------------*/
		String input = jobConf.get(INPUT_DIR);
		LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The input directory recorded is : " + input) ;
		String output = jobConf.get(OUTPUT_DIR);
		LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The output directory recorded is : " + output) ;

		InternalJob newContinuousJob = new InternalJob(jobID, new Path(
				submitDir), jobConf, input, output,
				jobXmlLocalFile, jobJarLocalFile,UserGroupInformation.getCurrentUser());

		//copy time value to ContinuousJob if this job is PartialJob
		//remember: only the first job in the dependency graph has time value
		if (jobConf.get(ContinuousJobConfig.DATE) != null)
			newContinuousJob.setTime(jobConf.get(ContinuousJobConfig.DATE));

		// Remember the current input size of continuous job. It is set to ContinuousJob object
		long length = fs.getContentSummary(new Path(jobConf.get(INPUT_DIR))).getLength();
		newContinuousJob.setInputSize(length);

		Path newInputPath = new Path(input);

		System.out.println("TUAN, OASIS-INRIA: Dependency of this Job: "
				+ jobConf.get(ContinuousJobConfig.DEPENDENT_JOB));


		// If the new job depends on the other jobs.
		// Build a dependency graph here
		if (jobConf.get(ContinuousJobConfig.DEPENDENT_JOB) != null)
		{
			String jobIDs = jobConf.get(ContinuousJobConfig.DEPENDENT_JOB);
			String[]jobIDList = jobIDs.split(",");

			for(int i=0; i<jobIDList.length; i++)
			{
				String dependentJobId = jobIDList[i];

				InternalJob cj = this.mapOfAllContinuousJob.get(dependentJobId);
				if (cj!=null){
					cj.getNextJobs().add(newContinuousJob);
					newContinuousJob.getPreviousJobs().add(cj);
				}
			}

		} else{
			// This job has no dependent jobs
			// Check if the input folder of this job is already appear in the
			// 		CJM or not.
			// If the input folder is already in CJM, add the job to the
			// 		associated list of continuous job for this input
			// If the input folder is not in CJM, create the new entry for it in
			// CJM
			Set<Path> setOfInputPath = this.continuousJobManager.keySet();
			for (Path p : setOfInputPath)
			{
				if (p.toString().equals(newInputPath.toString())) // two absolute paths are similar
				{
					this.continuousJobManager.get(p).add(newContinuousJob);
					this.mapOfAllContinuousJob.put(newContinuousJob.getJobID().toString(), newContinuousJob);

					System.out.println("-----------");
					System.out.println("TUAN, OASIS-INRIA: Size of "+ p.toString() + " is: " + this.continuousJobManager.get(p).size());
					System.out.println("-----------");
					return;
				}
			}

			// if the input path is new to the CJM, create the new entry for it
			Set<InternalJob> newSet = new HashSet<InternalJob>();
			newSet.add(newContinuousJob);
			this.continuousJobManager.put(newInputPath, newSet);

			this.mapOfAllContinuousJob.put(newContinuousJob.getJobID().toString(), newContinuousJob);

		}

		updateTimeStamp(newContinuousJob) ;
		System.out.println("addContinuousJob is STOPPED");
	}

	public void processCompleteJob(Path inputPath, String newFileStr) throws IOException,
	InterruptedException {
		LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : Process for CompleteJob STARTED.");
		Set<Path> setOfInput = this.continuousJobManager.keySet();
		if (setOfInput.size() > 0) {
			LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : There are jobs to process in the keyset.");
			long newLength = getLength(inputPath);

			Set<InternalJob> setContinuosJob = this.continuousJobManager.get(inputPath);

			System.out.println("Set of CJ: " + setContinuosJob.size());

			Iterator<InternalJob> iterator = setContinuosJob.iterator();

			while (iterator.hasNext())
			{
				final InternalJob cj = iterator.next();
				if (newLength > cj.getInputSize())
				{
					LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The job to process is being analyzed");
					cj.setInputSize(newLength);
					cj.setNewInputFilePath(newFileStr);

					// Submit all the application subscribe to this input in parallel.
					Thread thread = new Thread(){
						public void run(){
							try {

								// Submit the first job in the dependency graph
								LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The job is going to be submitted.");
								Job firstJob = submitContinuousJob(cj);
								LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The job has been submitted.");
								List<InternalJob> nextJobs = new LinkedList<InternalJob>();

								// add all the next jobs into the lists
								if(firstJob != null)
								{
									cj.setOutput(firstJob.getConfiguration().get(OUTPUT_DIR));
									for(int i=0; i<cj.getNextJobs().size(); i++)
									{
										InternalJob temp = cj.getNextJobs().get(i);
										temp.increaseVisitedValue();
										nextJobs.add(temp);
									}
								}

								// There is only one job in the dependency graph, try to update time for Partial Job
								if (nextJobs.size() == 0){
									// only update the time stamp for Partial Job
									updateTimeStamp(cj);

								}
								// while the list is not empty, continue to submit all the necessary jobs
								while (nextJobs.size()>0)
								{
									for (int i=0; i<nextJobs.size(); i++)
									{
										InternalJob temp = nextJobs.get(i);

										//all the precede jobs of temp are already executed or not?
										if(temp.getVisitedValue() == temp.getPreviousJobs().size())
										{
											Job submittedJob = submitContinuousJob(temp);
											if (submittedJob != null)
											{
												// reset the visited values of its previous jobs;
												temp.resetVisitedValue();

												// update new output path, since in the next jobs, they can read it as their input;
												temp.setOutput(submittedJob.getConfiguration().get(OUTPUT_DIR));

												nextJobs.remove(temp);

												//Am i the last job in the dependency graph?
												if(temp.getNextJobs().size() == 0){

													// update the time stamp for the first job in this dependency graph
													// only update the time stamp for Partial Job
													updateTimeStamp(cj);

												}else{
													for(int k=0; k<temp.getNextJobs().size(); k++)
													{
														InternalJob newCJ = temp.getNextJobs().get(k);

														// it is for checking that all the previous jobs of newCJ have been executed or not
														// checking it by comparing this value, to the number of its previous jobs.
														// if it is ==, means all the previous jobs have been executed
														// if it is <, means not all the previous jobs have been executed.
														newCJ.increaseVisitedValue();

														nextJobs.add(newCJ);
													}
												}
											}
										}
									}
								}
							} catch (IOException e) {
								LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : The resubmission of the job failed in the processCompleteJob method due to an IOException") ;
								e.printStackTrace();
							} catch (InterruptedException e) {
								LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : The resubmission of the job failed in the processCompleteJob method due to an InteruptedException") ;
								e.printStackTrace();
							}

						}
					};

					thread.setDaemon(true);
					thread.setPriority(Thread.MAX_PRIORITY);
					thread.start();
				}

			}
		}

	}


	private void updateTimeStamp(InternalJob firstJob) throws IOException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		firstJob.setTime(modifFmt.format(cal.getTime()));

		System.out.println("TUAN, OASIS-INRIA: Update time SUCCESSFULLY: " + firstJob.getJobID());
		System.out.println("		Time: " + firstJob.getTime());
	}


	// This method is called by method delete() and complete() in NameNode
	// org.apache.hadoop.hdfs.server.namenode.NameNode.java
	// Whenever there is a new file/folder appear in HDFS or an old file/folder
	// is deleted out of HDFS,
	// NameNode will call eventTrigger(Path of copied/deleted file) to inform to
	// CJM server.
	public synchronized void eventTrigger(final String newFileStr) throws IOException,
	InterruptedException {
		inputEvent = new String(newFileStr);

		String[] temp = newFileStr.split("/");

		// When the cluster is initialized by start-dfs, start-mapred, some
		// files are created building the cluster
		// Those file /temp and /jobtracker are created in local machine of
		// JobTracler. So we don't need to care about this path
		if (!temp[1].equals("tmp") && !temp[1].equals("jobtracker")) {
			LOG.info(" - PRAD MODIFIED - OASIS - INIRIA - : JobTracker tmp files created.") ;
			// Checking the path is belong to ContinuousJob or not.
			Set<Path> setOfInputCompleteJob = this.continuousJobManager.keySet();
			if(setOfInputCompleteJob.size()>0)
			{
				LOG.info(" - PRAD MODIFIED - OASIS - INIRIA - : The path loaded belongs to a continuous job.") ;
				Thread threadCompleteJob = new Thread()
				{
					public void run(){
						final Path inputForContinuousJob = isBelongToInputOfContinuousJob(new Path(newFileStr));

						// Checking for PartialJob
						if (inputForContinuousJob != null) {

							LOG.info(" - PRAD MODIFIED - OASIS - INIRIA - : There are new inputs for a continuous job.") ;
							try {
								if (fs.exists(inputForContinuousJob)) { 	// if the path is not existed,
									// it mean it was deleted,
									LOG.info(" - PRAD MODIFIED - OASIS - INIRIA - : The new files for the continuous job exists on the filesystem.") ;
									Thread thread = new Thread() { 		// So the job is deleted
										// out of CJM, do not
										// need to run it.
										public void run() { 			// If it is existing, it means,
											// there is a new increment in HDFS
											try { // So run the job again.
												LOG.info(" - PRAD MODIFIED - OASIS - INIRIA - : The job is going to be run again.") ;
												processCompleteJob(inputForContinuousJob, newFileStr);
												LOG.info(" - PRAD MODIFIED - OASIS - INIRIA - : The job has been re-run.") ;
											} catch (IOException e) {
												LOG.error(" - PRAD MODIFIED - OASIS - INIRIA - : The re-submission of the job failed due to an IOException from the method processCompleteJob.") ;
												e.printStackTrace();
											} catch (InterruptedException e) {
												LOG.error(" - PRAD MODIFIED - OASIS - INIRIA - : The re-submission of the job failed due to an InterruptedException from the method processCompleteJob.") ;
												e.printStackTrace();
											}
										}
									};

									thread.setDaemon(true);
									thread.setPriority(Thread.MAX_PRIORITY);
									thread.start();
								} else { 	// Path not existed in the HDFS anymore, so delete
									// all the job binding to that path
									LOG.error(" - PRAD MODIFIED - OASIS - INIRIA - : The path of the new inputs was not found on the filesystem.") ;
									continuousJobManager.remove(inputForContinuousJob);
								}
							} catch (IOException e) {
								LOG.error(" - PRAD MODIFIED - OASIS - INIRIA - : The re-submission of the job failed due to an IOException from the method exists.") ;
								e.printStackTrace();
							}
						}
					}
				};
				threadCompleteJob.setDaemon(true);
				threadCompleteJob.setPriority(Thread.MAX_PRIORITY);
				threadCompleteJob.start();
			}
		}
	}

	/**
	 *
	 *
	 */
	public static void main(String argv[]) throws Exception {

		System.out.println("TUAN, OASIS-INRIA: " + ContinuousJobTracker.class
				+ ", CJM SERVER IS STARTED");

		try {
			StringUtils.startupShutdownMessage(ContinuousJobTracker.class, argv,
					LOG);
			ContinuousJobTracker cjm = new ContinuousJobTracker();
			cjm.open();


		} catch (Throwable e) {
			System.out.println("TUAN, OASIS-INRIA: Can not create CJM server "
					+ e.toString());
			LOG.error(StringUtils.stringifyException(e));
			System.exit(-1);
		}
	}

	public long getProtocolVersion(String arg0, long arg1) throws IOException {
		return 0;
	}

	/**
	 * Resubmit the Continuous Job to the JobTracker
	 *
	 * @author VU Trong Tuan
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private Job submitContinuousJob(InternalJob continuousJob) throws IOException,
	InterruptedException {

		LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : submitContinuousJob is STARTED");
		JobStatus stat = null;
		Job job = null;
		try {

			if (continuousJob == null) {
				LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : continuousJob is null.");
			}

			Path jobJarLocalFile = continuousJob.getLocalJar();

			if (jobJarLocalFile == null) {
				LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : jobJarLocalFile is null.");
			}

			LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : Job Jar Path is: " + jobJarLocalFile.toString());

			JobConf jobConf = continuousJob.getJobConf();

			if (jobConf == null) {
				LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : jobConf is null.");
			}

			// No need to resubmit this job again to CJM
			jobConf.set(ContinuousJobConfig.NEED_TO_SUBMIT_TO_CJT, "false");

			// Set jar for executing the job
			System.out.println("Pat of Job Jar file: " + continuousJob.getLocalJar().toString());
			jobConf.setJar(continuousJob.getLocalJar().toString());

			String oldOutputFolder = continuousJob.getOrinalOutput();
			String inputFolder = continuousJob.getInputFolder();

			LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : oldOutputFolderIs : " + oldOutputFolder + " inputFolder is : " + inputFolder) ;

			jobConf.set(OUTPUT_DIR, generateContinuousOutputFolder(oldOutputFolder, jobConf));

			job = new Job(jobConf);


			if (jobConf.get(ContinuousJobConfig.DEPENDENT_JOB) != null) {	// second job in dependency graph

				LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : This is the second job.") ;
				// set the output of previous dependent jobs as its inputs.
				String dependentIdJobs = jobConf.get(ContinuousJobConfig.DEPENDENT_JOB);
				String temp [] = dependentIdJobs.split(",");

				for(int ii = 0; ii<temp.length; ii++)
				{
					System.out.println("Previous jobs: " + temp[ii]);
					InternalJob cj = this.mapOfAllContinuousJob.get(temp[ii]);

					if (cj != null)
					{
						System.out.println("	Output Folder of previous jobs: " + cj.getOutputFolder());

						// First dependent job, reset input for this job as output of previous dependent job.
						if (ii == 0){
							TextInputFormat.setInputPaths(job, new Path(cj.getOutputFolder()));

						}else{
							TextInputFormat.addInputPath(job, new Path(cj.getOutputFolder()));
						}
					}
				}
				//first job in dependency graph
			} else {
				LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : This job is the first job, time: " +continuousJob.getTime());

				//update the time stamp for the first job in dependency graph
				job.getConfiguration().set(ContinuousJobConfig.DATE, continuousJob.getTime());
				LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The conf of the first job is finished.") ;
			}

			LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The job is sent to Hadoop waitForCompletion.");

			UserGroupInformation tUgi = continuousJob.getUgi();
			if(tUgi!=null)
			{
				StringBuilder tBuilder = new StringBuilder();

				Field f = IntrospectionTool.getParentField(job.getClass(), "ugi");
				f.set(job, tUgi);

				for(String s : tUgi.getGroupNames())
				{
					tBuilder.append(s + ",");
				}

				LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : USER : " + tUgi.getUserName() + " Group :" + tBuilder + " JOBNAME : " + job.getJobName());
			}


			waitingForSubmit.remove(tUgi.getUserName() + "-" + job.getJobName());		
			job.submit();

			//IF A CALLBACK IS NEEDED WE DONE THE CALLBACK
			if(continuousJob.getJobConf().get(SubmissionCallBackProtocol.callBackProperty)!=null)
			{
				SubmissionCallBackProtocol callBackProtocol =

						(SubmissionCallBackProtocol)RPC.waitForProxy(SubmissionCallBackProtocol.class,

								ContinuousJobTrackerProtocol.versionID,

								NetUtils.createSocketAddr(continuousJob.getJobConf().get(SubmissionCallBackProtocol.callBackProperty)),

								new Configuration());

				LOG.info("BEFORE CALLBACK on " + continuousJob.getJobConf().get(SubmissionCallBackProtocol.callBackProperty));
				callBackProtocol.jobHasBeenResubmitted(); 
				LOG.info("CALLBACK DONE" + continuousJob.getJobConf().get(SubmissionCallBackProtocol.callBackProperty));
			}

			LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : The job has fully completed.");

		} catch (Exception e) {
			LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : Exception in submitContinuousJob: " + e.toString());
			LOG.error(" - PRAD MODIFIED - OASIS - INRIA - : " + e.getLocalizedMessage());
			e.printStackTrace() ;
		}

		LOG.info(" - PRAD MODIFIED - OASIS - INRIA - : submitContinuousJob is STOPPED");
		return job;
	}

	private long getLength(InternalJob continuousJob) throws IOException {
		FileSystem fs = FileSystem.get(new Configuration());
		return fs.getContentSummary(
				new Path(continuousJob.getJobConf().get(INPUT_DIR))).getLength();
	}

	private long getLength(Path path) throws IOException {
		return this.fs.getContentSummary(path).getLength();
	}


	/**
	 * Generate the unique name of the folder that will contain the output of the current sub job.
	 * @param str
	 * @param jobConf
	 * @return
	 */
	public static String generateContinuousOutputFolder(String str, JobConf jobConf) {
		DateFormat df = new SimpleDateFormat(ContinuousJobConfig.WRITABLE_DATE_FORMAT);
		String subJobDate = df.format(new Date()) ;
		jobConf.set(ContinuousJobConfig.DATE_OF_SUB_JOB, subJobDate) ;
		String newOutputFolder = str + "/" + "continuous-output_" + subJobDate ;

		return newOutputFolder;
	}


	/**
	 * Returns whether the folder specified is the continuous input folder or not
	 * @param path
	 * @return
	 */
	private Path isBelongToInputOfContinuousJob(Path path){
		System.out.println("TUAN, OASIS-INRIA: isBelongToInputOfContinuousJob is STARTED");
		Path temp = new Path(path.toString());
		Set<Path> setOfInput = this.continuousJobManager.keySet();

		while (temp!=null){
			for(Path p: setOfInput){
				String formattedPath = ContinuousUtil.removeHdfsSuffix(p.toString(), new Configuration());
				if(temp.toString().equals(formattedPath)){
					return p;
				}
			}
			temp = temp.getParent();

		}

		System.out.println("TUAN, OASIS-INRIA: isBelongToInputOfContinuousJob is STOPPED with result: "+ temp);
		return null;
	}

}
