package fr.inria.oasis.hadoop.continuous.callback;

import java.io.IOException;

import org.apache.hadoop.ipc.VersionedProtocol;

public interface SubmissionCallBackProtocol extends VersionedProtocol{

	public static final String callBackProperty = "mapred.continuous.callback.host";

	/**
	 * The SubmissionCallBackProtocol version.
	 */
	public static final long  versionID = 0L;

	/**
	 * Get the protocol version.
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public long getProtocolVersion ()
			throws IOException, InterruptedException;

	public void jobHasBeenResubmitted();

}
