package fr.inria.oasis.hadoop.continuous.config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Read every line of the input data then, put the time stamp at the end of every line.
 * Then the new data is written to /tmp/filename
 * After copy it to HDFS, /tmp/filename is deleted. The copy to HDFS and delete function are done at
 * org.apache.hadoop.fs.FileUtil.java
 * @author: VU Trong Tuan
 *
 */
public class EditTextFile {

	private final SimpleDateFormat modifFmt =
			new SimpleDateFormat(ContinuousJobConfig.DATE_FORMAT);

	private String src;
	private String modifiedTime;
	private String modifiledTimeStr;

	public EditTextFile(String src, Date time){
		this.src = src;

		Calendar cal = Calendar.getInstance();
		if (time == null){
			this.modifiedTime = new String (modifFmt.format(cal.getTime()));
		}else{
			this.modifiedTime = new String (modifFmt.format(time));
		}

	}

	public void modify(){
		this.modifiledTimeStr = new String(this.src+" "+ this.modifiedTime + "\n");
	}

	public String getModifiledTimeStr(){
		return this.modifiledTimeStr;
	}
}