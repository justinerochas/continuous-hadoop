package fr.inria.oasis.hadoop.continuous.job;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.server.namenode.ContinuousNameNode;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;
import org.apache.hadoop.util.LineReader;


import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;
import fr.inria.oasis.hadoop.continuous.server.TimeStamp;
import fr.inria.oasis.hadoop.continuous.util.ContinuousUtil;

/**
 * This class provides a specific format for continuous job.
 * Basically, this format acts like a TextInputFormat but takes care of reading and removing the timestamp in addition.
 *
 * @author Trong-Tuan Vu, Justine Rochas, Ludwig Poggi, Anthony Damiano, Clement Agarini
 */
public class ContinuousInputFormat {

	public static class CustomInputFormatForSortedFile extends FileInputFormat<Text, Text> {

		@Override
		public RecordReader<Text, Text> createRecordReader(InputSplit split,
				TaskAttemptContext context) throws IOException,
				InterruptedException {

			System.out.println(CustomInputFormatForSortedFile.class + " createRecordReader");
			RecordReaderForSortedFile rc = new RecordReaderForSortedFile();
			return rc;

		}


		public boolean isValidSplit(FileSplit split, JobContext context, List<TimeStamp> pTimeStampList)
		{
			JobConf jobConf= (JobConf) context.getConfiguration();
			String continuous = jobConf.get(ContinuousJobConfig.CONTINUOUS, "false");
			if (continuous.equals("true") && (jobConf.get(ContinuousJobConfig.DATE))!= null && !jobConf.get(ContinuousJobConfig.DATE).equals("")){


				DateFormat df = new SimpleDateFormat(ContinuousJobConfig.DATE_FORMAT);
				try {
					Date jobDate = df.parse(jobConf.get(ContinuousJobConfig.DATE));

					TimeStamp currentTimeStamp = pTimeStampList.get(pTimeStampList.size()-1);
					Date dateOfSplit = currentTimeStamp.getDate();

					int index = pTimeStampList.size();
					boolean splitIsAfter = true;

					while(index > 0 && ((splitIsAfter=dateOfSplit.after(jobDate))==true))
					{
						index --;
						currentTimeStamp = pTimeStampList.get(index);
						dateOfSplit = currentTimeStamp.getDate();
					}


					// If we have 0 this is because all timestamp for this split are after the jobDate
					// So this split is valid
					if(index == 0 && splitIsAfter)
					{
						//Check if the start of the split and the start of the timestamp are the same

						return true;
					}
					else
					{
						// this is because the file has been append so we need to check if split is between two timestamp range
						if((currentTimeStamp.getStart() < split.getStart()) && (split.getStart() < currentTimeStamp.getEnd()))
						{
							FileSplit tFileSplit;
							try {
								tFileSplit = new FileSplit(split.getPath(), currentTimeStamp.getEnd()+1, split.getLength(), split.getLocations());
								split=tFileSplit;
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								return false;
							}

							return true;
						}
						else
						{

							index++;
							currentTimeStamp = pTimeStampList.get(index-2);
							dateOfSplit = currentTimeStamp.getDate();
							//if split is not between two timestamp we need to check is split is little than one time stamp range
							// if this is the case the split is valid
							if(dateOfSplit.after(jobDate)&&(currentTimeStamp.getStart() < split.getStart()) && (split.getStart() < currentTimeStamp.getEnd()) )
							{
								return true;
							}


						}
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



			}



			return false;
		}



		/**
		 * Retrieves the splits from a set of files. If the file is low weigh, the file = 1 input split.
		 */
		@Override
		public List<InputSplit> getSplits(JobContext context)
				throws IOException {

			List<InputSplit> tmp = super.getSplits(context);

			System.out.println("SortedFileInput.CustomInputFormatForSortedFile.getSplits(), total splits: " + tmp.size() );
			List<InputSplit> list = new ArrayList<InputSplit>();
			if(tmp.size()>0)
			{
				JobConf jobConf= (JobConf) context.getConfiguration();

				String tInputDir = jobConf.get("mapred.input.dir");
				tInputDir = ContinuousUtil.removeHdfsSuffix(tInputDir,new Configuration());			
				MultiValueMap tMap = ContinuousNameNode.getAllTimeStampForDir(tInputDir);

				for (InputSplit is : tmp) {

					Object tmpObj = ((FileSplit) is).getPath();
					String tmpString = tmpObj.toString();
					tmpString = ContinuousUtil.removeHdfsSuffix(tmpString, new Configuration());
					Text tmpTextPath = new Text(tmpString);
					ArrayList<Object> tmpColl = (ArrayList<Object>) tMap.getCollection(tmpTextPath);
					if(isValidSplit((FileSplit) is, context, (ArrayList<TimeStamp>) (tmpColl.get(0))))
					{
						list.add(is);
					}
				}

				/****************** - PRAD MODIFIED - OASIS - INRIA - ******************/

				System.out.println("- PRAD MODIFIED - OASIS - INRIA - : The definitive list contains :") ;

				for (InputSplit is : list) {

					System.out.println(is) ;
				}

				System.out.println();

				/****************** - PRAD MODIFIED - OASIS - INRIA - END ******************/

				System.out.println("SortedFileInput.CustomInputFormatForSortedFile.getSplits(), selected splits: " + list.size());
			}
			return list;

		}



	}

	/**
	 * Provides practical way to read an input file.
	 */
	public static class RecordReaderForSortedFile extends RecordReader<Text, Text> {

		protected LineRecordReader lineReader;
		protected LongWritable lineKey;
		protected Text lineValue;
		protected Text key = new Text();
		protected Text value = new Text();
		private JobConf jobConf = null;


		@Override
		public void close() throws IOException {
			lineReader.close();
		}


		@Override
		public Text getCurrentKey() throws IOException, InterruptedException {
			return key;
		}


		@Override
		public Text getCurrentValue() throws IOException, InterruptedException {
			return value;
		}


		@Override
		public float getProgress() throws IOException, InterruptedException {
			return lineReader.getProgress();
		}


		@Override
		public void initialize(InputSplit split, TaskAttemptContext context)
				throws IOException, InterruptedException {

			lineReader = new LineRecordReader();
			lineReader.initialize((FileSplit) split, context);
			this.jobConf = (JobConf) context.getConfiguration();
		}


		/**
		 * {@inheritDoc}
		 * Takes care of removing the timestamp in addition.
		 */
		@Override
		public boolean nextKeyValue() throws IOException, InterruptedException {
			
			if (!lineReader.nextKeyValue()) {

				this.key = null;
				this.value = null;
				return false;
			}

			this.value = lineReader.getCurrentValue() ;
			return true ;
		}
	}
}

