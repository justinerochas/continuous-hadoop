package fr.inria.oasis.hadoop.continuous.config;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;


/**
 * Provides a format to read Text keys and integer values (which can be Long) from bytes in a carry file.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 */
public class TextNumericCarryFormat implements CarryFormat<Text, IntWritable>{


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Text readKey(byte[] keyBytes) {

		Text key = new Text() ;
		key.set(keyBytes) ;

		return key ;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public IntWritable readValue(byte[] valueBytes) {

		// The Text read
		Text valueText = new Text() ;
		valueText.set(valueBytes) ;

		// The LongWritable assumed
		IntWritable value = new IntWritable() ;
		String numberString = valueText.toString() ;
		int number = Integer.parseInt(numberString) ;
		value.set(number);

		return value;
	}
}
