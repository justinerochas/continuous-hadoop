package fr.inria.oasis.hadoop.continuous.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.net.NetUtils;

public abstract class ContinuousUtil {

	//TODO: Maybe it's a good idea to use only one porperty for host:port like it's done with job tracker

	/**
	 * Job tracker and continuous job tracker run on the same host so we do not need to add one property for each of them.
	 */

	public static String JOBTRACKER_HOST= "mapred.continuous.manager.control.host";
	/**
	 *
	 */
	public static String CONTINUOUSJOBTRACKER_PORT = "mapred.continuous.manager.control.port";

	private static final int MIN_AVAILABLE_PORT = 10000;

	private static final int MAX_AVAILABLE_PORT = 65535;

	private static int startPort = 10000;

	/**
	 * CJT will run on the host of JobTracker, at port 9021
	 *
	 * @author VU Trong Tuan
	 */

	public static InetSocketAddress getContinuousJobTrackerServerAddress(Configuration conf) {
		try {
			String address = conf.get(JOBTRACKER_HOST);
			if(address == null)
				address = InetAddress.getLocalHost().getCanonicalHostName();
			int port = conf.getInt(CONTINUOUSJOBTRACKER_PORT,9021);
			address += ":" + port;
			return NetUtils.createSocketAddr(address);

		}
		catch (Throwable t) {
			return NetUtils.createSocketAddr("localhost:9021");
		}
	}

	/**
	 * Find a free port on localhost between 10000 and 65535
	 * @return the free port found
	 * @throws Exception
	 */

	public static int findFreePort() throws Exception {
		int port = startPort;

		boolean portIsFree = false;
		ServerSocket server = null;

		while((port <= MAX_AVAILABLE_PORT) && !portIsFree)

		{
			try {
				server =  new ServerSocket(port);
				portIsFree = true;

			} catch (IOException e) {
				port += 1;
			}
		}

		if(!portIsFree)
			throw new Exception("No free port has been find.");

		if(!server.isClosed())
			server.close();

		startPort = port +1;
		return port;    
	}
	
	public static String removeHdfsSuffix(String pPath, Configuration pConf)
	{
		String suffix = pConf.get("fs.default.name");
		return pPath.split(suffix)[1];
	}
}
