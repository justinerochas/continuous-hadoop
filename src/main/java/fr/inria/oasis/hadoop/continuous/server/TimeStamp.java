package fr.inria.oasis.hadoop.continuous.server;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.math.Range;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class TimeStamp implements Writable{

	public static final String WRITABLE_DATE_FORMAT = "yyyyMMddHHmmss";
	
	private Date _date;
	private long start;
	private long end;

	public TimeStamp()
	{}
	
	public TimeStamp(Date _date, long start, long end) {
		super();
		this._date = _date;
		this.start = start;
		this.end = end;
	}
	
	private String getDateFormated()
	{
		return new SimpleDateFormat(TimeStamp.WRITABLE_DATE_FORMAT).format(_date);
	}
	
	public String getAttributes()
	{
		StringBuilder tBuilder = new StringBuilder();
		tBuilder.append(getDateFormated() + ";" + this.start + ";" + this.end);
		return tBuilder.toString();
	}
	
	public void setAttributes(String pString)
	{
		String[] pAttribute = pString.split(";");
		SimpleDateFormat tSDF = new SimpleDateFormat(TimeStamp.WRITABLE_DATE_FORMAT);
		try {
			_date = tSDF.parse(pAttribute[0]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start = Long.parseLong(pAttribute[1]);
		end = Long.parseLong(pAttribute[2]);
		
	}
	
	public Date getDate() {
		return _date;
	}
	public long getStart() {
		return start;
	}
	public long getEnd() {
		return end;
	}

	public long getSize()
	{
		return (end-start)+1;
	}
	
	@Override
	public boolean equals(Object obj) {
		TimeStamp t2 = obj instanceof TimeStamp?(TimeStamp)obj:null;

		if(obj!=null)
			if(t2.getDate().equals(this._date) && this.start==t2.getStart() && t2.getEnd()==this.end)
				return true;


		return false;
	}
	
	@Override
	public String toString() {

		return "DATE : " + this._date.toString() + ", START : " + this.start + ", END : " + this.end;
	}
	@Override
	public void readFields(DataInput in) throws IOException {
		start=in.readLong();
		end = in.readLong();
		SimpleDateFormat tSDF = new SimpleDateFormat(TimeStamp.WRITABLE_DATE_FORMAT);
		try {
			_date = tSDF.parse(in.readUTF());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(start);
		out.writeLong(end);
		SimpleDateFormat tSDF = new SimpleDateFormat(TimeStamp.WRITABLE_DATE_FORMAT);
		out.writeUTF(tSDF.format(_date));
	}
}