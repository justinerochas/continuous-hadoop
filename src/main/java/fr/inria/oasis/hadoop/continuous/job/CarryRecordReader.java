package fr.inria.oasis.hadoop.continuous.job;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.LineRecordReader;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import fr.inria.oasis.hadoop.continuous.config.CarryFormat;
import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;


/**
 * This class provides a specific reader to extract the data from the carry file.
 * Reads the data as following : key tab value.
 * The parameters are the type of the key and the type of the value.
 * The API set these parameters automatically with the mapred.mapoutput.key.class and mapred.mapoutput.Value.class conf attributes.
 * Each call to the "next" method moves the pointer forward in the carry file.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clement Agarini
 *
 * @param <KEYIN>
 * @param <VALUEIN>
 */
public class CarryRecordReader<KEYIN extends WritableComparable, VALUEIN extends Writable> extends RecordReader<KEYIN, VALUEIN> {

	private final LineRecordReader lineRecordReader;

	private CarryFormat<KEYIN, VALUEIN> format ;

	private byte separator ;
	private LongWritable dummyKey;
	private Text innerValue;

	private KEYIN currentKey ;
	private VALUEIN currentValue ;


	/**
	 * A CarryRecordreader must be contructed with a job configuration and a file split.
	 * @param conf
	 * @param split
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public CarryRecordReader(Configuration conf, FileSplit split, String separator) throws IOException {

		lineRecordReader = new LineRecordReader(conf, split);
		dummyKey = lineRecordReader.createKey();
		innerValue = lineRecordReader.createValue();
		this.separator = (byte) separator.charAt(0);

		Class<? extends CarryFormat<KEYIN, VALUEIN>> formatClass;

		try {
			// Trying to instanciate the formats
			formatClass = (Class<? extends CarryFormat<KEYIN, VALUEIN>>) Class.forName(conf.get(ContinuousJobConfig.CARRY_FORMAT));
			this.format = formatClass.newInstance() ;
		}

		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		catch (InstantiationException e) {
			e.printStackTrace();
		}

		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}



	/**
	 * Returns the claas of the key.
	 * @return
	 */
	public Class getKeyClass() {
		return currentKey.getClass() ;
	}


	/**
	 * Returns the class of the value.
	 * @return
	 */
	public Class getValueClass() {
		return currentValue.getClass() ;
	}


	/**
	 * Gets the current position of the reader in the carry file.
	 * @return
	 * @throws IOException
	 */
	public synchronized long getPos() throws IOException {
		return lineRecordReader.getPos();
	}


	/**
	 * Returns the position of the separator character in the current line read.
	 * @param utf
	 * @param start
	 * @param length
	 * @param sep
	 * @return
	 */
	public static int findSeparator(byte[] utf, int start, int length, byte sep) {
		for (int i = start; i < (start + length); i++) {
			if (utf[i] == sep) {
				return i;
			}
		}
		return -1;
	}


	/**
	 * Sets the parameters passed to the next key and value read in the carry file.
	 * @param key
	 * @param value
	 * @return
	 * @throws IOException
	 */
	public synchronized boolean next(KEYIN key, VALUEIN value)
			throws IOException {

		byte[] line = null;
		int lineLen = -1;

		if (lineRecordReader.next(dummyKey, innerValue)) {

			line = innerValue.getBytes();
			lineLen = innerValue.getLength();
		}

		else {

			return false;
		}

		if (line == null)
			return false;

		int pos = findSeparator(line, 0, lineLen, this.separator);

		// Key bytes
		int keyLen = pos;
		byte[] keyBytes = new byte[keyLen];
		System.arraycopy(line, 0, keyBytes, 0, keyLen);

		//Value bytes
		byte[] valBytes ;

		if (pos == -1) {

			valBytes = new byte[0] ;
		}

		else {

			int valLen = lineLen - keyLen - 1;
			valBytes = new byte[valLen];
			System.arraycopy(line, pos + 1, valBytes, 0, valLen);
		}

		key = format.readKey(keyBytes) ;
		value = format.readValue(valBytes) ;

		System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : CarryRecordReader - key is : " + key) ;
		System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : CarryRecordReader - value is : " + value) ;

		this.currentKey = key ;
		this.currentValue = value ;

		return true;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getProgress() throws IOException {
		return lineRecordReader.getProgress();
	}


	/**
	 * {@inheritDoc}
	 * Close the ressources.
	 */
	@Override
	public synchronized void close() throws IOException {
		lineRecordReader.close();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(InputSplit arg0, TaskAttemptContext arg1)
			throws IOException, InterruptedException {

	}


	/**
	 * {@inheritDoc}
	 * Not in use. Usenext(KEYIN key, VALUEIN value) instead.
	 */
	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		return false;
	}


	/**
	 * {@inheritDoc}
	 * Returns the current key to process.
	 */
	@Override
	public KEYIN getCurrentKey() throws IOException, InterruptedException {
		return currentKey ;
	}


	/**
	 * {@inheritDoc}
	 * Returns the current value to process.
	 */
	@Override
	public VALUEIN getCurrentValue() throws IOException, InterruptedException {
		return currentValue ;
	}
}
