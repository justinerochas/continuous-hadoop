package fr.inria.oasis.hadoop.continuous.job;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.PrivilegedExceptionAction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.ContinuousJobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

import fr.inria.oasis.hadoop.continuous.callback.SubmissionCallBackProtocol;
import fr.inria.oasis.hadoop.continuous.config.CarryFormat;
import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;
import fr.inria.oasis.hadoop.continuous.util.ContinuousUtil;
import fr.inria.oasis.hadoop.continuous.util.IntrospectionTool;

/**
 * This class provides an extension of Job for the continuous job implementation.
 * Extending this class you can make job re-executable when new data are put to the file system.
 *
 * {@inheritDoc}
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public class ContinuousJob extends Job{

	private boolean resubmissionCallBack = false;

	/**
	 * Method setting some properties needed by the continuous job into the job configuration.
	 * @throws IOException
	 */
	private void configureJob()
	{
		this.conf.setStrings(ContinuousJobConfig.NEED_TO_SUBMIT_TO_CJT, "true");
		this.conf.set(ContinuousJobConfig.CONTINUOUS, "true");

		// The job has no date first launching : it takes all the data. It is then updated.
		this.conf.setStrings(ContinuousJobConfig.DATE, "0000-00-0000:00:00");
		this.setInputFormatClass(ContinuousInputFormat.CustomInputFormatForSortedFile.class);

		//The default format to read the carry is Text key and LongWritable value.
		this.conf.set(ContinuousJobConfig.CARRY_FORMAT, ContinuousJobConfig.DEFAULT_CARRY_FORMAT) ;

		if(resubmissionCallBack)
		{
			try {
				this.conf.set(SubmissionCallBackProtocol.callBackProperty, InetAddress.getLocalHost().getHostAddress().toString() + ":" + ContinuousUtil.findFreePort());
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Default constructor which call super() and configureJob().
	 * @throws IOException
	 */
	public ContinuousJob(boolean resubmissionCallBack) throws IOException {
		super();
		this.resubmissionCallBack = resubmissionCallBack;
		configureJob();
	}

	/**
	 * Default constructor which call super() and configureJob().
	 * @throws IOException
	 */
	public ContinuousJob() throws IOException {
		super();
		configureJob();
	}


	/**
	 * Constructor with a specified job conf which call the equivalent parent constructor and configureJob.
	 * @param conf
	 * @throws IOException
	 */
	public ContinuousJob (Configuration conf) throws IOException
	{
		super(conf);
		configureJob();
	}
	/**
	 * Constructor with a specified job conf and a jobName which call the equivalent parent constructor and configureJob.
	 * @param conf
	 * @throws IOException
	 */
	public ContinuousJob (Configuration conf,String jobName) throws IOException
	{
		super(conf,jobName);
		configureJob();
	}

	public ContinuousJob (Configuration conf,String jobName,boolean resubmissionCallBack) throws IOException
	{
		super(conf,jobName);
		this.resubmissionCallBack = resubmissionCallBack;
		configureJob();
	}

	public void setDependency(ContinuousJob job) throws Exception{

		if (job == null)
			throw new Exception("Dependency job can not be null");

		// only the first job which dose not depends on any job has time, the others has no time
		this.resetToNonContinuousJob(this.conf);

		//set dependency
		this.conf.setStrings(ContinuousJobConfig.DEPENDENT_JOB, job.getJobID().toString());

		// get all the output of previous job as input of mine
		TextInputFormat.addInputPath(this, new Path(job.getConfiguration()
				.get("mapreduce.output.fileoutputformat.outputdir")));
	}

	/**
	 * Specify the folder to store carried data.
	 * @param carryPath : The path where the data are stored.
	 * @throws IOException
	 */
	public void setCarryOnFolder(String carryPath) throws IOException{
		this.conf.set(ContinuousJobConfig.CARRY_OUTPUT_PATH, carryPath);
	}


	public void setCarryFormat(Class<? extends CarryFormat> clazz) {
		this.conf.set(ContinuousJobConfig.CARRY_FORMAT, clazz.getName()) ;
	}


	/**
	 * Sets the name of the carry filesthat will be output
	 * It will be the name specifed + - + the number of the reduce that produced the carry file.
	 * @param carryFilesName
	 */
	public void setCarryFilesName(String carryFilesName) {
		this.conf.set(ContinuousJobConfig.CARRY_FILES_NAME, carryFilesName) ;
	}


	/**
	 * Sets the separator that is used to separate the keys and the values in the carry files.
	 * Warning : it must be one character only, if more, it will be truncated.
	 * Note : non printable character are allowed in the form "\" + letter code
	 * and unicode character are also accepted.
	 * @param separator
	 */
	public void setCarrySeparator(String separator) {
		this.conf.set(ContinuousJobConfig.CARRY_SEPARATOR, separator) ;
	}


	/**
	 * Set the current date into the job conf to keep the start time of the job.
	 * @param conf : the conf to update.
	 */
	private void setCurrentTime(JobConf conf){
		DateFormat df = new SimpleDateFormat(ContinuousJobConfig.DATE_FORMAT);
		Date date = Calendar.getInstance().getTime();
		conf.setStrings(ContinuousJobConfig.DATE, df.format(date));
	}


	/**
	 * Reset the job as a non continuous job.
	 * @param conf : the conf to update.
	 */
	private void resetToNonContinuousJob(JobConf conf){
		conf.setStrings(ContinuousJobConfig.CONTINUOUS, "false");
		conf.set(ContinuousJobConfig.DATE, "");
	}


	public void waitForResubmitionCompletion() throws Exception
	{
		if(resubmissionCallBack)
		{
			try {
				this.getJobClient().waitForResubmittedJobToComplete();
			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		else throw new Exception("Resubmission callback hasn't been enabled on creation. You can't wait for resubmission completion.");
	}

	/**
	 * Submit the job to the cluster and return immediately.
	 * @throws IOException
	 */
	public void submit() throws IOException, InterruptedException,
	ClassNotFoundException {
		ensureState(JobState.DEFINE);
		setUseNewAPI();

		// Connect to the JobTracker and submit the job
		connect();
		this.setInfo(getJobClient().submitJobInternal(conf));

		setJobID(this.getInfo().getID());

		this.setState(JobState.RUNNING);
	}


	/**
	 * {@inheritDoc}
	 *
	 * This method has been modified to set a ContinuousJobClient in place of the traditional jobClient.
	 * @throws IOException
	 * @throws InterruptedException
	 */

	private void connect() throws IOException, InterruptedException {
		ugi.doAs(new PrivilegedExceptionAction<Object>() {
			public Object run() throws IOException {
				//jobClient = new ContinuousJobClient();

				if(getJobClient() == null)
				{
					if(resubmissionCallBack)
						setJobClient(new ContinuousJobClient(true, conf.get(SubmissionCallBackProtocol.callBackProperty)));
					else setJobClient(new ContinuousJobClient());
				}
				return null;
			}
		});
	}


	/**
	 * Default to the new APIs unless they are explicitly set or the old mapper or
	 * reduce attributes are used.
	 * @throws IOException if the configuration is inconsistant
	 */
	private void setUseNewAPI() throws IOException {
		int numReduces = conf.getNumReduceTasks();
		String oldMapperClass = "mapred.mapper.class";
		String oldReduceClass = "mapred.reducer.class";
		conf.setBooleanIfUnset("mapred.mapper.new-api",
				conf.get(oldMapperClass) == null);
		if (conf.getUseNewMapper()) {
			String mode = "new map API";
			ensureNotSet("mapred.input.format.class", mode);
			ensureNotSet(oldMapperClass, mode);
			if (numReduces != 0) {
				ensureNotSet("mapred.partitioner.class", mode);
			} else {
				ensureNotSet("mapred.output.format.class", mode);
			}
		} else {
			String mode = "map compatability";
			ensureNotSet(JobContext.INPUT_FORMAT_CLASS_ATTR, mode);
			ensureNotSet(JobContext.MAP_CLASS_ATTR, mode);
			if (numReduces != 0) {
				ensureNotSet(JobContext.PARTITIONER_CLASS_ATTR, mode);
			} else {
				ensureNotSet(JobContext.OUTPUT_FORMAT_CLASS_ATTR, mode);
			}
		}
		if (numReduces != 0) {
			conf.setBooleanIfUnset("mapred.reducer.new-api",
					conf.get(oldReduceClass) == null);
			if (conf.getUseNewReducer()) {
				String mode = "new reduce API";
				ensureNotSet("mapred.output.format.class", mode);
				ensureNotSet(oldReduceClass, mode);
			} else {
				String mode = "reduce compatability";
				ensureNotSet(JobContext.OUTPUT_FORMAT_CLASS_ATTR, mode);
				ensureNotSet(JobContext.REDUCE_CLASS_ATTR, mode);
			}
		}
	}


	/**
	 * {@inheritDoc}
	 * @param state
	 * @throws IllegalStateException
	 */
	private void ensureState(JobState state) throws IllegalStateException {
		if (state != this.getState()) {
			throw new IllegalStateException("Job in state "+ this.getState() +
					" instead of " + state);
		}

		if (state == JobState.RUNNING && getJobClient() == null) {
			throw new IllegalStateException("Job in state " + JobState.RUNNING +
					" however jobClient is not initialized!");
		}
	}


	/**
	 * {@inheritDoc}
	 * @param attr
	 * @param msg
	 * @throws IOException
	 */
	private void ensureNotSet(String attr, String msg) throws IOException {
		if (conf.get(attr) != null) {
			throw new IOException(attr + " is incompatible with " + msg + " mode.");
		}
	}


	/**
	 * Setter used to set the private attribute jobId into the super, super class of this class (JobContext).
	 * @see JobContext
	 *
	 * @param pJobID : the jobId to set.
	 */
	public void setJobID(JobID pJobID)
	{
		Field tField = IntrospectionTool.getParentParentField(this.getClass(),"jobId");

		try {
			tField.set(this, pJobID);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Setter used to set the private attribute info into the super class (Job).
	 * @see Job
	 * @param pInfo : the info to set.
	 */
	public void setInfo(RunningJob pInfo)
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"info");

		try {
			tField.set(this, pInfo);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Getter used to get the private attribute info into the super class (Job).
	 * @see Job
	 * @return : The info attribute of the super class.
	 */
	public RunningJob getInfo()
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"info");

		try {
			return (RunningJob) tField.get(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * Setter used to set the private attribute state into the super class (Job).
	 * @see Job
	 * @param pState : the state to set.
	 */
	public void setState(JobState pState)
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"state");

		try {
			tField.set(this, pState);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Getter used to get the private attribute state state the super class (Job).
	 * @see Job
	 * @return The state of the job.
	 */
	public JobState getState()
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"state");

		try {
			return (JobState) tField.get(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * Getter used to get the private attribute jobClient the super class (Job).
	 * @see Job
	 * @return The jobclient of the job.
	 */
	public ContinuousJobClient getJobClient()
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"jobClient");

		try {
			return (ContinuousJobClient) tField.get(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * /**
	 * Setter used to set the private attribute jobClient the super class (Job).
	 * @see Job
	 * In that case we set a ContinuousJobClient in place of the jobclient because the continuous job client redefine some method to make hadoop being continuous.
	 * @param pJobClient : The job Client to set.
	 */
	public void setJobClient(ContinuousJobClient pJobClient)
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"jobClient");

		try {
			tField.set(this, pJobClient);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean waitForCompletion(boolean verbose) throws IOException, InterruptedException, ClassNotFoundException{

		// Ensure that the data will be read as continuous data with timestamp
		this.setInputFormatClass(ContinuousInputFormat.CustomInputFormatForSortedFile.class) ;

		// Then behave like a regular job
		return super.waitForCompletion(verbose) ;
	}
}
