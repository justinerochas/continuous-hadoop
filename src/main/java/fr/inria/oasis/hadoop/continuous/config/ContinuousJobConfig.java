package fr.inria.oasis.hadoop.continuous.config;

/**
 *
 * This class provides some static attributes representing properties name for the continuous job configuration,
 * and other constants that are needed
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 */

public class ContinuousJobConfig {


	/******** Not conf parameters ********/
	// Format of the timestamp
	public static final String DATE_FORMAT = "yyyy-MM-ddHH:mm:ss";

	// Format of the output folder name
	public static final String WRITABLE_DATE_FORMAT = "yyyyMMddHHmmss" ;

	// Default name of the carry output files
	public static final String DEFAULT_CARRY_FILES_NAME = "carry" ;

	// Default separator between the carry keys and the carry values
	public static final String DEFAULT_CARRY_SEPARATOR = "\t" ;

	// Default format to read the carry files
	public static final String DEFAULT_CARRY_FORMAT = TextNumericCarryFormat.class.getName() ;
	/*************************************/


	/******** Conf parameters ********/
	// Date of continuous job (i.e. of the first job)
	public static final String DATE = "mapred.continuous.job.date";

	// Date of the last sub job
	public static final String DATE_OF_SUB_JOB = "mapred.continuous.job.sub.date" ;

	// Specify if the job is continuous or not
	public static final String CONTINUOUS ="mapred.continuous.job";

	// Full path of the carry folder
	public static final String CARRY_OUTPUT_PATH = "mapreduce.continuous.job.carry.output.dir";

	// Simple name of the carry folder
	public static final String CARRY_ON_FOLDER = "mapreduce.continuous.job.carry.dir";

	// Name of the carry files (Note : the real name will be this one + - + the number of the reducer that generated it
	public static final String CARRY_FILES_NAME = "mapreduce.continuous.job.carry.files" ;

	// Separator between keys and values in the carry file
	public static final String CARRY_SEPARATOR = "mapreduce.continuous.job.carry.separator" ;

	// The format to read the carry file. Continuous Hadoop provides two of them, users must choose or implement their own.
	public static final String CARRY_FORMAT = "mapreduce.continuous.job.carry.format" ;

	// Says if the job is dependent of another job
	public static final String DEPENDENT_JOB = "mapred.continuous.job.dependent";

	// Says if the job has to be resubmitted to JobTracker
	public static final String NEED_TO_SUBMIT_TO_CJT = "mapred.continuous.job.need_to_resubmit";
	/*************************************/
}
