package fr.inria.oasis.hadoop.continuous.config;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;


/**
 * Represents the contract that a class which read a carry file should respect.
 * Most of the implementing classes have to fix parameters in order to recreate the object corresponding to the bytes read.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 * @param <KEY>
 * @param <VALUE>
 */
public interface CarryFormat<KEY extends WritableComparable, VALUE extends Writable> {


	/**
	 * Return the object key from its bytes read.
	 *
	 * @param keyBytes
	 * @return
	 */
	public abstract KEY readKey(byte[] keyBytes) ;


	/**
	 * Return the object value from its bytes read.
	 *
	 * @param valueBytes
	 * @return
	 */
	public abstract VALUE readValue(byte[] valueBytes) ;
}
