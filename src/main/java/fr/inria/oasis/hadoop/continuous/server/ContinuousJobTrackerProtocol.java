package fr.inria.oasis.hadoop.continuous.server;

import java.io.IOException;

import org.apache.hadoop.ipc.VersionedProtocol;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.JobID;
import org.apache.hadoop.security.Credentials;

/**
 * Define the methods a ContinuousJobTracker must implement
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public interface ContinuousJobTrackerProtocol extends VersionedProtocol{

	/**
	 * The ContinuousJobTracker version.
	 */
	public static final long  versionID = 0L;

	/**
	 * Get the protocol version.
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public long getProtocolVersion ()
			throws IOException, InterruptedException;

	/**
	 * Add a continuous job.
	 * @param jobConf
	 * @param jobName
	 * @param jobSubmitDir
	 * @param ts
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void addContinuousJob(JobConf jobConf, JobID jobName, String jobSubmitDir, Credentials ts)
			throws IOException, InterruptedException;

	/**
	 * Trigger the event.
	 * @param str
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void eventTrigger(String str)
			throws IOException, InterruptedException;

}
