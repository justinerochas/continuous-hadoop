package fr.inria.oasis.hadoop.continuous.mapreduce;

import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.RawKeyValueIterator;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.StatusReporter;
import org.apache.hadoop.mapreduce.TaskAttemptID;

import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;
import fr.inria.oasis.hadoop.continuous.job.CarryRecordWriter;
import fr.inria.oasis.hadoop.continuous.util.IntrospectionTool;

/**
 * This class override run in order to call continuous reduce and use ContinuousContext instead of an Hadoop Context
 * @author Clement Agarini, Justine Rochas, Anthony Damiano, Ludwig Poggi
 *
 * @param <KEYIN>
 * @param <VALUEIN>
 * @param <KEYOUT>
 * @param <VALUEOUT>
 */
public class ContinuousReducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT> extends Reducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT>  {
	private CarryRecordWriter<KEYIN, VALUEIN> carryWriter ;


	/**
	 * A continuous context is exactlt the as a context but contains a RecordWriter for the carry in addition.
	 * @author Clement Agarini, Justine Rochas, Anthony Damiano, Ludwig Poggi
	 *
	 */
	public class ContinuousContext extends Reducer.Context {

		org.apache.hadoop.mapred.RecordWriter<KEYIN, VALUEIN> carryRW;

		public ContinuousContext(Configuration conf, TaskAttemptID taskid,
				RawKeyValueIterator input,
				Counter inputKeyCounter,
				Counter inputValueCounter,
				RecordWriter<KEYOUT,VALUEOUT> output,
				OutputCommitter committer,
				StatusReporter reporter,
				RawComparator<KEYIN> comparator,
				Class<KEYIN> keyClass,
				Class<VALUEIN> valueClass,
				org.apache.hadoop.mapred.RecordWriter<KEYIN, VALUEIN> carryRW) throws IOException, InterruptedException
				{
			super(conf, taskid, input, inputKeyCounter, inputValueCounter,output, committer, reporter, comparator, keyClass, valueClass);
			this.carryRW = carryRW;
				}

		public void carry (KEYIN key, VALUEIN value) {
			try {
				this.carryRW.write(key, value);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}


	}

	@SuppressWarnings("unchecked")
	protected void continuousReduce(KEYIN key, Iterable<VALUEIN> values, ContinuousContext context) throws IOException, InterruptedException {
		for(VALUEIN value: values) {
			context.write((KEYOUT) key, (VALUEOUT) value);
		}
	}

	private org.apache.hadoop.mapred.RecordWriter<KEYIN, VALUEIN> createCarryRecordWriter(Context context) {
		// Creating the writer
		JobConf conf = (JobConf) context.getConfiguration() ;
		String carryPath = conf.get(ContinuousJobConfig.CARRY_OUTPUT_PATH) ;

		// Creating the name of the carry file
		String[] taskID = context.getTaskAttemptID().toString().split("_");
		String carryFilesName = conf.get(ContinuousJobConfig.CARRY_FILES_NAME, ContinuousJobConfig.DEFAULT_CARRY_FILES_NAME) ;
		String carryFilePath = carryFilesName + "-" + taskID[4].substring(1) ;

		// Creating the record writer
		String carrySeparator = conf.get(ContinuousJobConfig.CARRY_SEPARATOR, ContinuousJobConfig.DEFAULT_CARRY_SEPARATOR) ;
		this.carryWriter = new CarryRecordWriter<KEYIN, VALUEIN>(carryPath + "/" + carryFilePath, carrySeparator, conf) ;
		org.apache.hadoop.mapred.RecordWriter<KEYIN, VALUEIN> carryRecordWriter = carryWriter.getWriter() ;
		return carryRecordWriter;

	}

	/**
	 * Create a ContinuousContext using the initial context
	 * @param context
	 * @param conf
	 * @return
	 */
	private ContinuousContext createContinuousContext(Context context, JobConf conf) {
		//get all fields of the initial context and creates a continuous context using all these fields
		ContinuousContext continuousContext = null;
		org.apache.hadoop.mapred.RecordWriter<KEYIN, VALUEIN> carryRecordWriter = this.createCarryRecordWriter(context);
		Class<KEYIN> pKeyClass = null;
		Class<VALUEIN> pValueClass = null;

		Configuration pConf = (Configuration) IntrospectionTool.getRealObjectFromField(context, "conf");
		TaskAttemptID pTaskid = (TaskAttemptID) IntrospectionTool.getRealObjectFromField(context, "taskId");
		RawKeyValueIterator pInput = (RawKeyValueIterator) IntrospectionTool.getRealObjectFromField(context, "input");
		Counter pInputKeyCounter = (Counter) IntrospectionTool.getRealObjectFromField(context, "inputKeyCounter");
		Counter pInputValueCounter = (Counter) IntrospectionTool.getRealObjectFromField(context, "inputValueCounter");
		RecordWriter<KEYOUT, VALUEOUT> pOutput = (RecordWriter<KEYOUT, VALUEOUT>) IntrospectionTool.getRealObjectFromField(context, "output");
		StatusReporter pReporter = (StatusReporter) IntrospectionTool.getRealObjectFromField(context, "reporter");
		RawComparator<KEYIN> pComparator = (RawComparator<KEYIN>) IntrospectionTool.getRealObjectFromField(context, "comparator");

		try {
			pKeyClass = (Class<KEYIN>) Class.forName(conf.get("mapred.mapoutput.key.class"));
			pValueClass = (Class<VALUEIN>) Class.forName(conf.get("mapred.mapoutput.value.class"));
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			//one argument uses a getter instead of instrospection
			this.reInitializeValueIterator(context);
			continuousContext = new ContinuousContext(pConf, pTaskid, pInput, pInputKeyCounter, pInputValueCounter, pOutput, context.getOutputCommitter(), pReporter, pComparator, pKeyClass, pValueClass, carryRecordWriter);
		}
		catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return continuousContext;
	}

	private void reInitializeValueIterator (Context context) {
		//when created(in the constructor) the context set a variable so we have to  set it to its previous value

		try {
			//The context has a field named input and this field has an other field named rawIter
			//We want to set rawIter to null so we use introspection

			Field f1 = context.getClass().getSuperclass().getDeclaredField("input");
			f1.setAccessible(true);
			Class c2 = null ;
			Field f2 = f1.get(context).getClass().getDeclaredField("val$rawIter");
			f2.setAccessible(true);
			c2 = f2.get(f1.get(context)).getClass();
			Field f3 = c2.getDeclaredField("minSegment");
			f3.setAccessible(true);
			f3.set(f2.get(f1.get(context)), null);
		}

		catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void run(Context context) throws IOException, InterruptedException {
		//Create a continuous context
		JobConf conf = (JobConf) context.getConfiguration();
		ContinuousContext continuousContext = this.createContinuousContext(context, conf);

		// Hadoop behaviour
		setup(continuousContext);
		while (continuousContext.nextKey()) {
			continuousReduce((KEYIN)continuousContext.getCurrentKey(), continuousContext.getValues(), continuousContext);
		}
		cleanup(continuousContext);

		this.carryWriter.close() ;
	}
}
