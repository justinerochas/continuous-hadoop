package fr.inria.oasis.hadoop.continuous.callback;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.RPC.Server;
import org.apache.hadoop.net.NetUtils;

public class SubmissionCallBack implements SubmissionCallBackProtocol {


	public static final Log LOG = LogFactory.getLog(SubmissionCallBack.class.getName());

	private Object parent;
	private Server server;

	public SubmissionCallBack(Object parent)
	{
		this.parent = parent;
	}

	public void open(String pHostPort)
	{

		InetSocketAddress serverAddress = NetUtils.createSocketAddr(pHostPort);
		try {
			this.server = RPC.getServer(this, serverAddress.getHostName(),
					serverAddress.getPort(), new Configuration());

			this.server.start();

		} catch (IOException e) {
			LOG.error(e);
			e.printStackTrace();
		}

		System.out.println("	PRAD, OASIS_INRIA: " + SubmissionCallBackProtocol.class
				+ " Server is STARTED ");
	}

	public void close()
	{
		this.server.stop();
	}

	@Override
	public synchronized void jobHasBeenResubmitted() {
		synchronized (parent) {
			parent.notify();
		}
	}

	@Override
	public long getProtocolVersion() throws IOException, InterruptedException {
		return SubmissionCallBackProtocol.versionID;
	}

	@Override
	public long getProtocolVersion(String arg0, long arg1) throws IOException {
		return SubmissionCallBackProtocol.versionID;
	}

}
