package fr.inria.oasis.hadoop.continuous.mapreduce;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.util.ReflectionUtils;

import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;
import fr.inria.oasis.hadoop.continuous.job.CarryRecordReader;



/**
 * The ContinuousMapper class is the class to extends instead of Mapper when client writes a continuous job.
 * It manages to output what is in the carry file with a good format to send it to the reducer.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clement Agarini
 *
 * @param <KEYIN>
 * @param <VALUEIN>
 * @param <KEYOUT>
 * @param <VALUEOUT>
 */
public class ContinuousMapper<KEYIN extends WritableComparable, VALUEIN extends Writable, KEYOUT extends WritableComparable, VALUEOUT extends Writable>  extends Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT> {

	private Class<KEYOUT> keyClass ;
	private Class<VALUEOUT> valueClass ;


	/**
	 * {@inheritDoc}
	 * Also produces in output the keys values read in the carry file.
	 * @param context
	 * @throws IOException
	 */
	@Override
	public void run(Context context) throws IOException, InterruptedException {

		System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : ContinuousMapper is launched.") ;

		super.run(context) ;

		// Getting the carry files to be processed by this mapper
		Path[] carryFilesPath = carryFilesToLoad(context) ;

		if (carryFilesPath.length > 0) {

			// Ouput the data of these carry files
			outputCarryKeysValues(context, carryFilesPath) ;
		}
	}


	/**
	 * Reads the keys and values contained in the carry file with a CarryRecordReader
	 * And send them to the reducer via the context specified.
	 *
	 * @param context
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void outputCarryKeysValues(Context context, Path[] carryFilesPath) throws IOException, InterruptedException {

		JobConf jobConf = (JobConf) context.getConfiguration() ;

		try {

			// Getting the class of the key and the value
			keyClass = (Class<KEYOUT>) Class.forName(jobConf.get("mapred.mapoutput.key.class")) ;
			valueClass = (Class<VALUEOUT>) Class.forName(jobConf.get("mapred.mapoutput.value.class")) ;
		}

		catch (ClassNotFoundException e1) {

			System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : The class of the key or of the value couldn't be loaded.") ;
			e1.printStackTrace();
		}

		KeyValueTextInputFormat format = new KeyValueTextInputFormat() ;
		format.configure(jobConf) ;

		// Adding the path of the carry file in the current path to read. Carry filename must always be the same. Client has no choice about it.

		KeyValueTextInputFormat.setInputPaths(jobConf, carryFilesPath) ;

		// Getting the splits corresponding to the carry files to load
		InputSplit[] inputSplit = format.getSplits(jobConf, 1) ;

		for (int i = 0 ; i < inputSplit.length ; i++) {

			// Defining the separator to read the carry file
			String separator = jobConf.get(ContinuousJobConfig.CARRY_SEPARATOR, ContinuousJobConfig.DEFAULT_CARRY_SEPARATOR) ;

			// Created the custom record reader with the input split to read.
			CarryRecordReader<KEYOUT, VALUEOUT> reader = new CarryRecordReader<KEYOUT, VALUEOUT>(jobConf, (FileSplit) inputSplit[i], separator) ;

			System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Keys and Values of carry are ready to be read.") ;

			// Creating the initial key and value objects
			KEYOUT key = (KEYOUT) ReflectionUtils.newInstance(keyClass,jobConf) ;
			VALUEOUT value = (VALUEOUT) ReflectionUtils.newInstance(valueClass,jobConf) ; ;

			while (reader.next(key, value)) {

				System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Type of key is " + key.getClass().getName()) ;
				System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Key written is " + reader.getCurrentKey()) ;
				System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Type of value is " + value.getClass().getName()) ;
				System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Value written is " + reader.getCurrentValue()) ;

				context.write(reader.getCurrentKey(), reader.getCurrentValue()) ;
			}
		}
	}


	/**
	 * Determines which carry blocks files must load each mapper.
	 * The carry files are equally balanced between the mappers.
	 * 
	 * @param context
	 * @return
	 */
	private Path[] carryFilesToLoad(Context context) {

		JobConf jobConf = (JobConf) context.getConfiguration() ;

		// Carry files path selected for this mapper
		ArrayList<Path> paths = new ArrayList<Path>() ;

		// List of all the carry files
		ArrayList<Path> carryFiles = new ArrayList<Path>() ;

		try {

			Path carryFolder = new Path(jobConf.get(ContinuousJobConfig.CARRY_OUTPUT_PATH)) ;

			// Getting the current filesystem
			FileSystem fs = FileSystem.get(jobConf) ;

			if (fs.exists(carryFolder)) {

				FileStatus[] filesStatus = fs.listStatus(carryFolder) ;

				// Filling the list of carry files name
				for (FileStatus fileStatus : filesStatus) {

					carryFiles.add(fileStatus.getPath()) ;
				}

				// Declaring the figures needed to give the right carry file to the right mapper
				int numberOfMaps = jobConf.getNumMapTasks() ;
				int numberOfCarryFiles = filesStatus.length ;
				int numberOfCarryFilesPerMap = numberOfCarryFiles / numberOfMaps ;
				System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Number of carry files per map is " + numberOfCarryFilesPerMap) ;

				// Getting the ID of the current mapper
				String[] mapTaskID = context.getTaskAttemptID().toString().split("_");

				// Getting this current mapper ID
				int mapID = Integer.parseInt(mapTaskID[4]) ;
				System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Map ID is " + mapID) ;

				// Getting the index of the first carry file to load
				int currentCarryToload = mapID ;

				// Adding in the list all the carry files to load for this mapper
				for (int i = 0 ; i < numberOfCarryFilesPerMap ; i++) {

					paths.add(carryFiles.get(currentCarryToload)) ;
					currentCarryToload += numberOfMaps  ;
				}

				// Adding the pending carry file if there is one this current mapper
				int indexOfPendingFile = numberOfMaps * numberOfCarryFilesPerMap + mapID ;

				if (indexOfPendingFile < carryFiles.size()) {

					System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : One pending file has been added in the input path of this mapper.") ;
					paths.add(carryFiles.get(indexOfPendingFile)) ;
				}
			}
		}

		catch (IOException e) {
			e.printStackTrace();
		}

		for (Path p : paths) {
			System.out.println(" - PRAD MODIFIED - OASIS - INRIA - : Path loaded is " + p) ;
		}

		return paths.toArray(new Path[0]) ;
	}
}
