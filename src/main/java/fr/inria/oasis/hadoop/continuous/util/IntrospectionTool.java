package fr.inria.oasis.hadoop.continuous.util;

import java.lang.reflect.Field;
import java.util.ArrayList;


/**
 * This class provide static methods to access field with introspection.
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public class IntrospectionTool {

	/**
	 * Get the specified field in the super class of the specified class.
	 * Field returned is set to accessible true.
	 * @param pClass
	 * @param pName
	 * @return
	 */
	public static Field getParentField(Class pClass, String pName)
	{
		Class c = pClass.getSuperclass();

		Field tField=null;
		try {
			tField = c.getDeclaredField(pName);
			tField.setAccessible(true);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		return  tField;
	}

	/**
	 * Get the specified field in the super super class of the specified class.
	 * Field returned is set to accessible true.
	 * @param pClass
	 * @param pName
	 * @return
	 */

	public static Field getParentParentField(Class pClass, String pName)
	{
		Class c = pClass.getSuperclass().getSuperclass();

		Field tField=null;
		try {
			tField = c.getDeclaredField(pName);
			tField.setAccessible(true);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		return  tField;
	}

	/**
	 * Get all fields in the hierarchy of the parameter obj.
	 *
	 * @param obj
	 */

	public static ArrayList<Field> getAllFields(Object obj) {
		ArrayList<Field> resFieldList = new ArrayList<Field>();
		Field[] tmpFieldArray;
		Class c = obj.getClass();

		tmpFieldArray = c.getDeclaredFields();
		for(int i=0 ; i<tmpFieldArray.length ; i++) {
			resFieldList.add(tmpFieldArray[i]);
		}

		while(c != Object.class) {
			c = c.getSuperclass();
			tmpFieldArray = c.getDeclaredFields();
			for(int i=0 ; i<tmpFieldArray.length ; i++) {
				resFieldList.add(tmpFieldArray[i]);
			}
		}
		return resFieldList;
	}

	/**
	 * Get the field which is named "name" in the hierarchy of the object ojb
	 * @param obj
	 * @param name
	 * @return
	 */

	public static Field getFieldInHierarchy (Object obj, String name) {

		Field resField = null;
		ArrayList<Field> fieldList = IntrospectionTool.getAllFields(obj);

		for (Field tmpField : fieldList) {
			if(tmpField.getName().equals(name)) {
				resField = tmpField;
			}
		}
		return resField;
	}

	/**
	 * Return the Object that is represented by the Field name on the Object obj. if the field is not accessible, the method make it accessible
	 * @param obj
	 * @param name
	 * @return
	 */
	public static Object getRealObjectFromField(Object obj, String name) {
		Object res = null;

		Field tmpField = getFieldInHierarchy(obj, name);
		tmpField.setAccessible(true);
		try {
			res = tmpField.get(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return res;
	}
}
