package org.apache.hadoop.mapred;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.PrivilegedExceptionAction;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.filecache.TrackerDistributedCacheManager;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.hadoop.hdfs.DFSClient;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.mapred.QueueManager.QueueACL;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.JobSubmissionFiles;
import org.apache.hadoop.mapreduce.security.TokenCache;
import org.apache.hadoop.mapreduce.split.JobSplitWriter;
import org.apache.hadoop.net.NetUtils;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.security.authorize.AccessControlList;
import org.apache.hadoop.security.token.Token;
import org.apache.hadoop.util.ReflectionUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import fr.inria.oasis.hadoop.continuous.callback.SubmissionCallBack;
import fr.inria.oasis.hadoop.continuous.callback.SubmissionCallBackProtocol;
import fr.inria.oasis.hadoop.continuous.server.ContinuousJobTracker;
import fr.inria.oasis.hadoop.continuous.server.ContinuousJobTrackerProtocol;
import fr.inria.oasis.hadoop.continuous.util.ContinuousUtil;
import fr.inria.oasis.hadoop.continuous.util.IntrospectionTool;

/**
 * Provide a continuous implementation of the jobClient the most important modification is the submitjobinternal method.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public class ContinuousJobClient extends JobClient {

	private static final Log LOG = LogFactory.getLog(JobClient.class);
	private SubmissionCallBack callBackSubmitter;
	private String hostPort = null;
	private boolean waitForAResubmission = false;

	public ContinuousJobClient()
	{
		super();
	}

	public ContinuousJobClient(boolean waitForAResubmission,String pHostPort)
	{
		callBackSubmitter = new SubmissionCallBack(this);
		hostPort = pHostPort;
		callBackSubmitter.open(this.hostPort);
	}


	/**
	 * Wait until the resubmitted job is not finished.
	 * !!!!!!!######!!!!!! CALL THIS METHOD ONLY ONCE THE JOB HAS BEEN RESUBMITTED !!!!!!!######!!!!!!
	 *
	 * @param pUserName : the username of the resubmitted job
	 * @param pJobName : the resubmitted job name
	 * @param pJobTrackerHost : the host of the job tracker (Not continuous job tracker just job tracker)
	 * @param pJobTrackerPort : the port of the job tracker (Not continuous job tracker just job tracker)
	 * @throws Exception
	 */
	public void waitForResubmittedJobToComplete() throws Exception
	{

		synchronized (this) {
			this.wait();
		}

		JobSubmissionProtocol jobSubmissionProtocol = (JobSubmissionProtocol) RPC.getProxy(JobSubmissionProtocol.class,
				JobSubmissionProtocol.versionID, JobTracker.getAddress(getConf()),
				UserGroupInformation.getCurrentUser(), getConf(),
				NetUtils.getSocketFactory(getConf(), JobSubmissionProtocol.class));

		JobConf jobConf = (JobConf) getConf();

		String tJobName = jobConf.getJobName();
		String tUserName =  jobConf.getUser();

		JobStatus[] tStatus = jobSubmissionProtocol.jobsToComplete();
		int i = tStatus.length -1;
		boolean jobFound = false;
		JobStatus j =null;

		while(i>=0 && !jobFound)
		{
			j = tStatus[i];
			if(j.getUsername().equals(tUserName)&&jobSubmissionProtocol.getJobProfile(j.getJobID()).getJobName().equals(tJobName))
				jobFound = true;
			i--;
		}

		if(i==0 && !jobFound && tStatus.length > 0)
		{
			throw new Exception("No job submitted for the user JOBLAUNCHED" + tStatus.length);
		}

		else if(tStatus.length == 0)
			throw new Exception("There is no job submitted (for your user and for others)");
		else
		{
			// Connect to the equivalent job tracker to get job status
			while(!jobSubmissionProtocol.getJobStatus(j.getJobID()).isJobComplete())
			{
				Thread.sleep(5000);
				LOG.info("JOB IS NOT COMPLETE.");
			}
		}

		if(i==0 && !jobFound && tStatus.length > 0)
		{
			throw new Exception("No job submitted for the user JOBLAUNCHED" + tStatus.length);
		}

		else if(tStatus.length == 0)
			throw new Exception("There is no job submitted (for your user and for others)");
		else
		{
			// Connect to the equivalent job tracker to get job status
			while(!jobSubmissionProtocol.getJobStatus(j.getJobID()).isJobComplete())
			{
				Thread.sleep(5000);
				LOG.info("JOB IS NOT COMPLETE.");
			}
		}
	}

	/**
	 * Internal method for submitting jobs to the system.
	 * @param job the configuration to submit
	 * @return a proxy object for the running job
	 * @throws FileNotFoundExceptionjobClient
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public
	RunningJob submitJobInternal(final JobConf job
			) throws FileNotFoundException,
			ClassNotFoundException,
			InterruptedException,
			IOException {

		if(waitForAResubmission)
			job.set(SubmissionCallBackProtocol.callBackProperty, hostPort);

		// Saving the folder of the continuous output folders
		final String oldOutput = job.get(ContinuousJobTracker.OUTPUT_DIR) ;

		// Setting the output of the first job to a generated naming folder
		job.set(ContinuousJobTracker.OUTPUT_DIR, ContinuousJobTracker.generateContinuousOutputFolder(job.get(ContinuousJobTracker.OUTPUT_DIR), job));

		setConf(job);
		init(job);

		LOG.info(" USER AND GROUP INFO ; "+ getUgi().getUserName() + " " + getUgi().getGroupNames() + " ");
		return getUgi().doAs(new PrivilegedExceptionAction<RunningJob>() {
			public RunningJob run() throws FileNotFoundException,
			ClassNotFoundException,
			InterruptedException,
			IOException{
				JobConf jobCopy = job;
				Path jobStagingArea = JobSubmissionFiles.getStagingDir(ContinuousJobClient.this,
						jobCopy);
				JobID jobId = getJobSubmitClient().getNewJobId();

				//PRAD modified
				System.out.println("CONTINUOUS TUAN, OASIS-INRIA: " + JobClient.class + ": JobID is " + jobId);

				// TUAN-COMMENT: create the empty directory for the job
				Path submitJobDir = new Path(jobStagingArea, jobId.toString());
				jobCopy.set("mapreduce.job.dir", submitJobDir.toString());
				JobStatus status = null;

				try {

					populateTokenCache(jobCopy, jobCopy.getCredentials());

					copyAndConfigureFiles(jobCopy, submitJobDir);

					// TUAN-COMMENT: try to get the size of the job folder  FileSystem.get
					// get delegation token for the dir
					TokenCache.obtainTokensForNamenodes(jobCopy.getCredentials(),
							new Path [] {submitJobDir},
							jobCopy);
					//TUAN COMMENT:
					System.out.println("TUAN, OASIS-INRIA: submitDir is: " + submitJobDir.toString());

					Path submitJobFile = JobSubmissionFiles.getJobConfPath(submitJobDir);
					int reduces = jobCopy.getNumReduceTasks();

					//Load config to retrieve the continuous jobtracker
					job.setJobSubmitHostAddress(ContinuousUtil.getContinuousJobTrackerServerAddress(jobCopy).getAddress().toString());
					job.setJobSubmitHostName(ContinuousUtil.getContinuousJobTrackerServerAddress(jobCopy).getHostName().toString());

					JobContext context = new JobContext(jobCopy, jobId);

					jobCopy = (JobConf)context.getConfiguration();

					// Check the output specification
					if (reduces == 0 ? jobCopy.getUseNewMapper() :
						jobCopy.getUseNewReducer()) {
						org.apache.hadoop.mapreduce.OutputFormat<?,?> output =
								ReflectionUtils.newInstance(context.getOutputFormatClass(),
										jobCopy);
						output.checkOutputSpecs(context);
					} else {
						jobCopy.getOutputFormat().checkOutputSpecs(getFs(), jobCopy);
					}

					// Create the splits for the job
					FileSystem fs = submitJobDir.getFileSystem(jobCopy);
					LOG.debug("Creating splits at " + fs.makeQualified(submitJobDir));

					int maps = writeSplits(context, submitJobDir);
					jobCopy.setNumMapTasks(maps);

					// write "queue admins of the queue to which job is being submitted"
					// to job file.
					String queue = jobCopy.getQueueName();
					AccessControlList acl = getJobSubmitClient().getQueueAdmins(queue);
					jobCopy.set(QueueManager.toFullPropertyName(queue,
							QueueACL.ADMINISTER_JOBS.getAclName()), acl.getACLString());

					// Write job file to JobTracker's fs
					FSDataOutputStream out =
							FileSystem.create(fs, submitJobFile,
									new FsPermission(JobSubmissionFiles.JOB_FILE_PERMISSION));

					try {
						jobCopy.writeXml(out);
					} finally {
						out.close();
					}
					//
					// Now, actually submit the job (using the submit name)
					//
					// TUAN-COMMENT: really submit the job to the cluster
					printTokens(jobId, jobCopy.getCredentials());
					status = getJobSubmitClient().submitJob(
							jobId, submitJobDir.toString(), jobCopy.getCredentials());
					JobProfile prof = getJobSubmitClient().getJobProfile(jobId);

					/************************* OASIS-INRIA ******************************/
					String NEED_TO_SUBMIT_TO_CJT = "mapred.continuous.job.need_to_resubmit";
					//PRAD MODIFIED
					System.out.println("CALLING submitJobInternal");
					String needToSubmitToCJM = job.get(NEED_TO_SUBMIT_TO_CJT);
					if( needToSubmitToCJM != null){
						if(needToSubmitToCJM.equals("true")){
							ContinuousJobTrackerProtocol continuousClient = (ContinuousJobTrackerProtocol)RPC.waitForProxy(ContinuousJobTrackerProtocol.class,
									ContinuousJobTrackerProtocol.versionID,
									ContinuousUtil.getContinuousJobTrackerServerAddress(job),
									job);
							System.out.println("OASIS --- SUBMIT JOBDIR = " + submitJobDir);

							// Restauring the highest output folder
							job.set(ContinuousJobTracker.OUTPUT_DIR, oldOutput) ;

							// Adding the continuous job
							continuousClient.addContinuousJob((JobConf) job, jobId, submitJobDir.toString(), null);
						}
					}

					//PRAD MODIFIED

					if (status != null && prof != null) {
						return new NetworkedJob(status, prof, getJobSubmitClient());
					} else {
						throw new IOException("Could not launch job");
					}
				}
				catch(Exception ex)
				{
					System.out.println(ex);
					return null;
				}
				finally
				{
					if (status == null) {
						LOG.info("Cleaning up the staging area " + submitJobDir);

						if (getFs() != null && submitJobDir != null)
						{
							getFs().delete(submitJobDir, true);
						}
					}
				}
			}
		});
	}


	/**
	 * configure the jobconf of the user with the command line options of
	 * -libjars, -files, -archives
	 * @param job the JobConf
	 * @param submitJobDir
	 * @throws IOException
	 */
	private void copyAndConfigureFiles(JobConf job, Path jobSubmitDir)
			throws IOException, InterruptedException {
		short replication = (short)job.getInt("mapred.submit.replication", 10);
		copyAndConfigureFiles(job, jobSubmitDir, replication);

		// Set the working directory
		if (job.getWorkingDirectory() == null) {
			job.setWorkingDirectory(getFs().getWorkingDirectory());
		}
	}

	private void copyAndConfigureFiles(JobConf job, Path submitJobDir,
			short replication) throws IOException, InterruptedException {

		if (!(job.getBoolean("mapred.used.genericoptionsparser", false))) {
			LOG.warn("Use GenericOptionsParser for parsing the arguments. " +
					"Applications should implement Tool for the same.");
		}

		// Retrieve command line arguments placed into the JobConf
		// by GenericOptionsParser.
		String files = job.get("tmpfiles");
		String libjars = job.get("tmpjars");
		String archives = job.get("tmparchives");

		//
		// Figure out what fs the JobTracker is using.  Copy the
		// job to it, under a temporary name.  This allows DFS to work,
		// and under the local fs also provides UNIX-like object loading
		// semantics.  (that is, if the job file is deleted right after
		// submission, we can still run the submission to completion)
		//

		// Create a number of filenames in the JobTracker's fs namespace
		FileSystem fs = submitJobDir.getFileSystem(job);
		LOG.debug("default FileSystem: " + fs.getUri());
		if (fs.exists(submitJobDir)) {
			throw new IOException("Not submitting job. Job directory " + submitJobDir
					+" already exists!! This is unexpected.Please check what's there in" +
					" that directory");
		}
		submitJobDir = fs.makeQualified(submitJobDir);
		FsPermission mapredSysPerms = new FsPermission(JobSubmissionFiles.JOB_DIR_PERMISSION);
		FileSystem.mkdirs(fs, submitJobDir, mapredSysPerms);
		Path filesDir = JobSubmissionFiles.getJobDistCacheFiles(submitJobDir);
		Path archivesDir = JobSubmissionFiles.getJobDistCacheArchives(submitJobDir);
		Path libjarsDir = JobSubmissionFiles.getJobDistCacheLibjars(submitJobDir);
		// add all the command line files/ jars and archive
		// first copy them to jobtrackers filesystem

		if (files != null) {
			FileSystem.mkdirs(fs, filesDir, mapredSysPerms);
			String[] fileArr = files.split(",");
			for (String tmpFile: fileArr) {
				URI tmpURI;
				try {
					tmpURI = new URI(tmpFile);
				} catch (URISyntaxException e) {
					throw new IllegalArgumentException(e);
				}
				Path tmp = new Path(tmpURI);
				Path newPath = copyRemoteFiles(fs,filesDir, tmp, job, replication);
				try {
					URI pathURI = getPathURI(newPath, tmpURI.getFragment());
					DistributedCache.addCacheFile(pathURI, job);
				} catch(URISyntaxException ue) {
					//should not throw a uri exception
					throw new IOException("Failed to create uri for " + tmpFile, ue);
				}
				DistributedCache.createSymlink(job);
			}
		}

		if (libjars != null) {
			FileSystem.mkdirs(fs, libjarsDir, mapredSysPerms);
			String[] libjarsArr = libjars.split(",");
			for (String tmpjars: libjarsArr) {
				Path tmp = new Path(tmpjars);
				Path newPath = copyRemoteFiles(fs, libjarsDir, tmp, job, replication);
				DistributedCache.addArchiveToClassPath
				(new Path(newPath.toUri().getPath()), job, fs);
			}
		}


		if (archives != null) {
			FileSystem.mkdirs(fs, archivesDir, mapredSysPerms);
			String[] archivesArr = archives.split(",");
			for (String tmpArchives: archivesArr) {
				URI tmpURI;
				try {
					tmpURI = new URI(tmpArchives);
				} catch (URISyntaxException e) {
					throw new IllegalArgumentException(e);
				}
				Path tmp = new Path(tmpURI);
				Path newPath = copyRemoteFiles(fs, archivesDir, tmp, job, replication);
				try {
					URI pathURI = getPathURI(newPath, tmpURI.getFragment());
					DistributedCache.addCacheArchive(pathURI, job);
				} catch(URISyntaxException ue) {
					//should not throw an uri excpetion
					throw new IOException("Failed to create uri for " + tmpArchives, ue);
				}
				DistributedCache.createSymlink(job);
			}
		}

		// First we check whether the cached archives and files are legal.
		TrackerDistributedCacheManager.validate(job);
		//  set the timestamps of the archives and files
		TrackerDistributedCacheManager.determineTimestamps(job);
		//  set the public/private visibility of the archives and files
		TrackerDistributedCacheManager.determineCacheVisibilities(job);
		// get DelegationTokens for cache files
		TrackerDistributedCacheManager.getDelegationTokens(job,
				job.getCredentials());

		String originalJarPath = job.getJar();

		if (originalJarPath != null) {           // copy jar to JobTracker's fs
			// use jar name if job is not named.
			if ("".equals(job.getJobName())){
				job.setJobName(new Path(originalJarPath).getName());
			}
			Path submitJarFile = JobSubmissionFiles.getJobJar(submitJobDir);
			job.setJar(submitJarFile.toString());
			fs.copyFromLocalFile(new Path(originalJarPath), submitJarFile);
			fs.setReplication(submitJarFile, replication);
			fs.setPermission(submitJarFile,
					new FsPermission(JobSubmissionFiles.JOB_FILE_PERMISSION));
		} else {
			LOG.warn("No job jar file set.  User classes may not be found. "+
					"See JobConf(Class) or JobConf#setJar(String).");
		}
	}

	/** copies a file to the jobtracker filesystem and returns the path where it was copied to
	 * @param jtFs
	 * @param parentDir
	 * @param originalPath
	 * @param job
	 * @param replication
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private Path copyRemoteFiles(FileSystem jtFs, Path parentDir,
			final Path originalPath, final JobConf job, short replication)
					throws IOException, InterruptedException {
		//check if we do not need to copy the files
		// is jt using the same file system.
		// just checking for uri strings... doing no dns lookups
		// to see if the filesystems are the same. This is not optimal.
		// but avoids name resolution.

		FileSystem remoteFs = null;
		remoteFs = originalPath.getFileSystem(job);

		if (compareFs(remoteFs, jtFs)) {
			return originalPath;
		}
		// this might have name collisions. copy will throw an exception
		//parse the original path to create new path
		Path newPath = new Path(parentDir, originalPath.getName());
		FileUtil.copy(remoteFs, originalPath, jtFs, newPath, false, job);
		jtFs.setReplication(newPath, replication);
		return newPath;
	}

	/** see if two file systems are the same or not
	 *
	 */
	private boolean compareFs(FileSystem srcFs, FileSystem destFs) {
		URI srcUri = srcFs.getUri();
		URI dstUri = destFs.getUri();
		if (srcUri.getScheme() == null) {
			return false;
		}
		if (!srcUri.getScheme().equals(dstUri.getScheme())) {
			return false;
		}
		String srcHost = srcUri.getHost();
		String dstHost = dstUri.getHost();
		if ((srcHost != null) && (dstHost != null)) {
			try {
				srcHost = InetAddress.getByName(srcHost).getCanonicalHostName();
				dstHost = InetAddress.getByName(dstHost).getCanonicalHostName();
			} catch(UnknownHostException ue) {
				return false;
			}
			if (!srcHost.equals(dstHost)) {
				return false;
			}
		}
		else if (srcHost == null && dstHost != null) {
			return false;
		}
		else if (srcHost != null && dstHost == null) {
			return false;
		}
		//check for ports
		if (srcUri.getPort() != dstUri.getPort()) {
			return false;
		}
		return true;
	}

	private URI getPathURI(Path destPath, String fragment)
			throws URISyntaxException {
		URI pathURI = destPath.toUri();
		if (pathURI.getFragment() == null) {
			if (fragment == null) {
				pathURI = new URI(pathURI.toString() + "#" + destPath.getName());
			} else {
				pathURI = new URI(pathURI.toString() + "#" + fragment);
			}
		}
		return pathURI;
	}

	@SuppressWarnings("unchecked")
	private <T extends InputSplit>
	int writeNewSplits(JobContext job, Path jobSubmitDir) throws IOException,
	InterruptedException, ClassNotFoundException {
		Configuration conf = job.getConfiguration();
		InputFormat<?, ?> input =
				ReflectionUtils.newInstance(job.getInputFormatClass(), conf);

		List<InputSplit> splits = input.getSplits(job);
		T[] array = (T[]) splits.toArray(new InputSplit[splits.size()]);

		// sort the splits into order based on size, so that the biggest
		// go first
		Arrays.sort(array, new SplitComparator());
		JobSplitWriter.createSplitFiles(jobSubmitDir, conf,
				jobSubmitDir.getFileSystem(conf), array);
		return array.length;
	}

	private int writeSplits(org.apache.hadoop.mapreduce.JobContext job,
			Path jobSubmitDir) throws IOException,
			InterruptedException, ClassNotFoundException {
		JobConf jConf = (JobConf)job.getConfiguration();
		int maps;
		if (jConf.getUseNewMapper()) {
			maps = writeNewSplits(job, jobSubmitDir);
		} else {
			maps = writeOldSplits(jConf, jobSubmitDir);
		}
		return maps;
	}

	//method to write splits for old api mapper.
	private int writeOldSplits(JobConf job, Path jobSubmitDir)
			throws IOException {
		org.apache.hadoop.mapred.InputSplit[] splits =
				job.getInputFormat().getSplits(job, job.getNumMapTasks());
		// sort the splits into order based on size, so that the biggest
		// go first
		Arrays.sort(splits, new Comparator<org.apache.hadoop.mapred.InputSplit>() {
			public int compare(org.apache.hadoop.mapred.InputSplit a,
					org.apache.hadoop.mapred.InputSplit b) {
				try {
					long left = a.getLength();
					long right = b.getLength();
					if (left == right) {
						return 0;
					} else if (left < right) {
						return 1;
					} else {
						return -1;
					}
				} catch (IOException ie) {
					throw new RuntimeException("Problem getting input split size", ie);
				}
			}
		});
		JobSplitWriter.createSplitFiles(jobSubmitDir, job,
				jobSubmitDir.getFileSystem(job), splits);
		return splits.length;
	}

	@SuppressWarnings("unchecked")
	private void printTokens(JobID jobId,
			Credentials credentials) throws IOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Printing tokens for job: " + jobId);
			for(Token<?> token: credentials.getAllTokens()) {
				if (token.getKind().toString().equals("HDFS_DELEGATION_TOKEN")) {
					LOG.debug("Submitting with " +
							DFSClient.stringifyToken((Token<org.apache.hadoop.hdfs.security.token.delegation.DelegationTokenIdentifier>) token));
				}
			}
		}
	}

	@Override
	protected void finalize() throws Throwable {
		try
		{
			this.callBackSubmitter.close();
		}
		finally
		{
			super.finalize();
		}
	}

	@SuppressWarnings("unchecked")
	private void readTokensFromFiles(Configuration conf, Credentials credentials
			) throws IOException {
		// add tokens and secrets coming from a token storage file
		String binaryTokenFilename =
				conf.get("mapreduce.job.credentials.binary");
		if (binaryTokenFilename != null) {
			Credentials binary =
					Credentials.readTokenStorageFile(new Path("file:///" +
							binaryTokenFilename), conf);
			credentials.addAll(binary);
		}
		// add secret keys coming from a json file
		String tokensFileName = conf.get("mapreduce.job.credentials.json");
		if(tokensFileName != null) {
			LOG.info("loading user's secret keys from " + tokensFileName);
			String localFileName = new Path(tokensFileName).toUri().getPath();

			boolean json_error = false;
			try {
				// read JSON
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> nm =
						mapper.readValue(new File(localFileName), Map.class);

				for(Map.Entry<String, String> ent: nm.entrySet()) {
					credentials.addSecretKey(new Text(ent.getKey()),
							ent.getValue().getBytes());
				}
			} catch (JsonMappingException e) {
				json_error = true;
			} catch (JsonParseException e) {
				json_error = true;
			}
			if(json_error)
				LOG.warn("couldn't parse Token Cache JSON file with user secret keys");
		}
	}

	//get secret keys and tokens and store them into TokenCache
	@SuppressWarnings("unchecked")
	private void populateTokenCache(Configuration conf, Credentials credentials)
			throws IOException{
		readTokensFromFiles(conf, credentials);

		// add the delegation tokens from configuration
		String [] nameNodes = conf.getStrings(JobContext.JOB_NAMENODES);
		LOG.debug("adding the following namenodes' delegation tokens:" +
				Arrays.toString(nameNodes));
		if(nameNodes != null) {
			Path [] ps = new Path[nameNodes.length];
			for(int i=0; i< nameNodes.length; i++) {
				ps[i] = new Path(nameNodes[i]);
			}
			TokenCache.obtainTokensForNamenodes(credentials, ps, conf);
		}
	}

	private static class SplitComparator implements Comparator<InputSplit> {
		@Override
		public int compare(InputSplit o1, InputSplit o2) {
			try {
				long len1 = o1.getLength();
				long len2 = o2.getLength();
				if (len1 < len2) {
					return 1;
				} else if (len1 == len2) {
					return 0;
				} else {
					return -1;
				}
			} catch (IOException ie) {
				throw new RuntimeException("exception in compare", ie);
			} catch (InterruptedException ie) {
				throw new RuntimeException("exception in compare", ie);
			}
		}
	}

	public JobSubmissionProtocol getJobSubmitClient()
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"jobSubmitClient");

		try {
			return (JobSubmissionProtocol) tField.get(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	public UserGroupInformation getUgi()
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"ugi");
		try {
			return (UserGroupInformation) tField.get(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}
	public UserGroupInformation setUgi(UserGroupInformation pUserGroupInfo)
	{
		Field tField = IntrospectionTool.getParentField(this.getClass(),"ugi");
		try {
			tField.set(this, pUserGroupInfo);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}




}
