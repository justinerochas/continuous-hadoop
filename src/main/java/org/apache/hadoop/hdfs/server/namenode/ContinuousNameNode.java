package org.apache.hadoop.hdfs.server.namenode;

import java.awt.TextArea;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.protocol.ClientProtocol;
import org.apache.hadoop.hdfs.server.common.HdfsConstants.StartupOption;
import org.apache.hadoop.hdfs.server.protocol.DatanodeProtocol;
import org.apache.hadoop.hdfs.server.protocol.NamenodeProtocol;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.DataInputBuffer;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.GenericWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.mapred.JobTracker;
import org.apache.hadoop.metrics2.lib.DefaultMetricsSystem;
import org.apache.hadoop.net.NetUtils;
import org.apache.hadoop.security.RefreshUserMappingsProtocol;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.security.authorize.RefreshAuthorizationPolicyProtocol;
import org.apache.hadoop.util.StringUtils;

import fr.inria.oasis.hadoop.continuous.server.ContinuousJobTrackerProtocol;
import fr.inria.oasis.hadoop.continuous.server.TimeStamp;
import fr.inria.oasis.hadoop.continuous.util.ContinuousUtil;
import fr.inria.oasis.hadoop.continuous.util.IntrospectionTool;


/**
 * The continuousNameNode replace the NameNode to add the continuous implementation and to use a continuousJobTracker in place of the job tracker.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public class ContinuousNameNode  extends NameNode implements TimeStampProtocol{

	private MultiValueMap timeStampMap;


	public void addSrcTimeStamp(String pSrc, long pFileSize, long blockSize) {
		
		// if it's a new file we add a new key 
		if(!this.timeStampMap.containsKey(pSrc)) {	
			ArrayList<TimeStamp> tListTimeStamp = new ArrayList<TimeStamp>();
			if(pFileSize > blockSize) {
				long tmpSize = 0;
				while((tmpSize + blockSize) < pFileSize) {
					tmpSize = tmpSize + blockSize;
					TimeStamp tTimeStamp = new TimeStamp(new Date(), tmpSize - blockSize, tmpSize);
					tListTimeStamp.add(tTimeStamp);
				}
				
				TimeStamp timeStamp = new TimeStamp(new Date(), tmpSize, pFileSize);
				tListTimeStamp.add(timeStamp);
			}
			
			else {
				TimeStamp timeStamp = new TimeStamp(new Date(), 0, pFileSize-1);
				tListTimeStamp.add(timeStamp);
			}
			this.timeStampMap.put(pSrc,tListTimeStamp);
		}
		
		//it should be the append case so the key already exists in the map
		else {
			ArrayList<TimeStamp> tListTimeStamp = (ArrayList<TimeStamp>) timeStampMap.get(pSrc);
			long pTimeStampSize = 0;

			// find the size of the current file 
			for(TimeStamp t:tListTimeStamp) {
				pTimeStampSize += t.getSize();
			}

			// If the new size is bigger than the old one we need to add a new timestamp
			if(pFileSize > pTimeStampSize) {
				TimeStamp tTimeStamp = new TimeStamp(new Date(), pTimeStampSize-1, pFileSize-1);
				this.timeStampMap.put(pSrc, tTimeStamp);
			}
		}
	}

	public List<TimeStamp> getListTimeStampForFile(String pSrc)
	{
		return (List<TimeStamp>) this.timeStampMap.get(pSrc);
	}


	private MapWritable convertMultiValueMapOfStringTimeStampToMapWritable(MultiValueMap pMapToConvert) {
		MapWritable tMWritable = new MapWritable();
		Iterator iterator = pMapToConvert.keySet().iterator();
		
		while(iterator.hasNext()) {
			
			String key = (String) iterator.next();
			ArrayList<Object> tmp = (ArrayList<Object>) pMapToConvert.getCollection(key);
			ArrayList<TimeStamp> tTimeStampArray = (ArrayList<TimeStamp>) tmp.get(0);
			StringBuilder tBuilder = new StringBuilder();
			
			for(TimeStamp tStimeStamp:tTimeStampArray) {
				
				tBuilder.append(tStimeStamp.getAttributes() + "|");
			}
			
			Text t = new Text(tBuilder.toString());
			tMWritable.put(new Text(key), t);
		}
		
		return tMWritable;
	}
	
	public static MultiValueMap convertMapWritableOfStringTimeStampToMultiValueMap(MapWritable pMapToConvert)
	{
		MultiValueMap tMVMap = new MultiValueMap();
		Set<Writable> sw = pMapToConvert.keySet();
		
		Iterator iterator = pMapToConvert.keySet().iterator();
		
		while(iterator.hasNext())
		{
			Text key = (Text) iterator.next();
			
			boolean b = pMapToConvert.containsKey(key);
			Text text = (Text) pMapToConvert.get(key);
			String tmpS = text.toString();
			String[] tArrayOfString = tmpS.split("[|]");
			
			ArrayList<TimeStamp> tTimeStampArray = new ArrayList<TimeStamp>();
			
			for(String s:tArrayOfString)
			{
				
				TimeStamp tTimeStamp = new TimeStamp();
				tTimeStamp.setAttributes(s);
				tTimeStampArray.add(tTimeStamp);
			}
			
			tMVMap.put(key,tTimeStampArray);
		}
		
		return tMVMap;
	}
	
	public static MultiValueMap getAllTimeStampForDir(String pDir)
	{


			TimeStampProtocol timeStampProtocol;
			try {
				timeStampProtocol = (TimeStampProtocol) RPC.waitForProxy(TimeStampProtocol.class, TimeStampProtocol.versionID, ContinuousNameNode.getAddress(new Configuration()),new Configuration());
				
				
				MapWritable tM = timeStampProtocol.getListOfTimeStampForDir(pDir);

				return convertMapWritableOfStringTimeStampToMultiValueMap(tM);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			


		return null;

	}


	public MapWritable getListOfTimeStampForDir(String pDir)
	{
		MultiValueMap tReturnMap = null;

		try {
			FileSystem tFs  = FileSystem.get(new Configuration());
			Path tPath = new Path(pDir);

			FileStatus tFileStatuts = tFs.getFileStatus(tPath);

			if(!tFileStatuts.isDir())
				return null;

			ArrayList<String> pListOfSrc = (ArrayList<String>) this.getAllFileRecursively(tFs, tPath);
			
			tReturnMap = new MultiValueMap();
			
			for(String s: pListOfSrc)
			{
				tReturnMap.putAll(s, timeStampMap.getCollection(s));
			}
			
			return convertMultiValueMapOfStringTimeStampToMapWritable(tReturnMap);


		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private List<String> getAllFileRecursively(FileSystem pFs, Path pPath)
	{

		List<String> tListOfSrc = new ArrayList<String>();

		try {
			FileStatus[] tFileStatus = pFs.listStatus(pPath);
			Configuration tConf = new Configuration();
			for(FileStatus f : tFileStatus)
			{
				if(!f.isDir())
				{
					tListOfSrc.add(ContinuousUtil.removeHdfsSuffix(f.getPath().toString(),tConf));
				}
				else
				{
					tListOfSrc.addAll(getAllFileRecursively(pFs, f.getPath()));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return tListOfSrc;
	}

	/**
	 * Create a new ContinuousNameNode with the specified conf
	 * @param conf : the conf to use.
	 * @throws IOException
	 */
	public ContinuousNameNode(Configuration conf) throws IOException {
		super(conf);
		this.timeStampMap = new MultiValueMap();
	}

	/*************************OASIS-INRIA******************************/

	/**
	 * The continuousJobTrackerProtocol to use.
	 */
	public static ContinuousJobTrackerProtocol continuousClient = null;

	/** {@inheritDoc} */
	public boolean complete(String src, String clientName) throws IOException {

		/******************* - PRAD MODIFIED - OASIS - INRIA - ******************/
		long localstart = System.currentTimeMillis();
		boolean fileComplete = false;

		while (!fileComplete) {
			fileComplete = super.complete(src, clientName);
			if (!fileComplete) {
				try {
					Thread.sleep(400);
					if (System.currentTimeMillis() - localstart > 5000) {
						LOG.info("Could not complete file " + src + " retrying...");
					}
				}
				catch (InterruptedException ie) {

				}
			}
		}

		System.out.println("TUAN, OASIS-INRIA: " + NameNode.class +

				" complete method is STARTED: " +

				" complete an file entry in the namespace," +

				" filename: " + src +

				", clientName: " + clientName);

		try {

			if (continuousClient != null){

				//TimeStamp the data
				FileSystem fs = FileSystem.get(new Configuration());
				Path srcPath = new Path(src);
				FileStatus tFstatus = fs.getFileStatus(srcPath);

				// If this is not a dir we get the size and timestamp the data
				if(!tFstatus.isDir())
				{
					long tFileSize = fs.getFileStatus(new Path(src)).getLen();
					long blockSize = fs.getFileStatus(srcPath).getBlockSize();
					this.addSrcTimeStamp(src, tFileSize, blockSize);
				}

				// Send an event to CJT for job resubmission
				System.out.println("Complete: Trigger the CJM by client");
				continuousClient.eventTrigger(src);
				System.out.println("Complete: RPC finished");
			}

		} catch (Throwable e) {
			System.out.println("Exception in complete: "+ e.getCause().toString());
			System.out.println("Exception in complete: "+ e.getCause().getMessage());
			System.out.println("Exception in complete: "+ e.getMessage());
		}

		/*************************OASIS-INRIA******************************/

		return true;
	}

	/** {@inheritDoc} */
	public boolean delete(String src, boolean recursive) throws IOException {

		boolean retParent=false;
		retParent = super.delete(src, recursive);

		/************************* OASIS-INRIA ******************************/

		System.out.println("TUAN, OASIS-INRIA: " + NameNode.class +
				" delete method is STARTED: " +
				" delete an file entry in the namespace," +
				" filename: " + src +
				", recursive: " + recursive);

		try {
			if (continuousClient != null){

				//Remove timestamp info
				this.timeStampMap.remove(src);

				System.out.println("Delete: Trigget the CJM by client");
				continuousClient.eventTrigger(src);
				System.out.println("Delete: RPC finished");
			}

		} catch (Throwable e) {
			System.out.println("Exception in delete: "+ e.getCause().toString());
			System.out.println("Exception in delete: "+ e.getCause().getMessage());
			System.out.println("Exception in delete: "+ e.getMessage());
		}
		return retParent;
	}

	/************************* OASIS-INRIA ******************************/

	/******************* - PRAD MODIFIED - OASIS - INRIA - *****************/


	private static StartupOption parseArguments(String args[]) {

		int argsLen = (args == null) ? 0 : args.length;
		StartupOption startOpt = StartupOption.REGULAR;

		for(int i=0; i < argsLen; i++) {
			String cmd = args[i];
			if (StartupOption.FORMAT.getName().equalsIgnoreCase(cmd)) {
				startOpt = StartupOption.FORMAT;
			}
			else if (StartupOption.REGULAR.getName().equalsIgnoreCase(cmd))
			{
				startOpt = StartupOption.REGULAR;

			}
			else if (StartupOption.UPGRADE.getName().equalsIgnoreCase(cmd)) {
				startOpt = StartupOption.UPGRADE;
			}
			else if (StartupOption.ROLLBACK.getName().equalsIgnoreCase(cmd)) {
				startOpt = StartupOption.ROLLBACK;
			}
			else if (StartupOption.FINALIZE.getName().equalsIgnoreCase(cmd)) {
				startOpt = StartupOption.FINALIZE;
			}
			else if (StartupOption.IMPORT.getName().equalsIgnoreCase(cmd)) {
				startOpt = StartupOption.IMPORT;
			}
			else
				return null;
		}

		return startOpt;
	}

	private static void printUsage() {

		System.err.println(
				"Usage: java NameNode [" +
						StartupOption.FORMAT.getName() + "] | [" +
						StartupOption.UPGRADE.getName() + "] | [" +
						StartupOption.ROLLBACK.getName() + "] | [" +
						StartupOption.FINALIZE.getName() + "] | [" +
						StartupOption.IMPORT.getName() + "]");
	}

	private static void setStartupOption(Configuration conf, StartupOption opt) {
		conf.set("dfs.namenode.startup", opt.toString());
	}

	/**
	 * Verify that configured directories exist, then
	 * Interactively confirm that formatting is desired
	 * for each existing directory and format them.
	 *
	 * @param conf
	 * @param isConfirmationNeeded
	 * @return true if formatting was aborted, false otherwise
	 * @throws IOException
	 */

	private static boolean format(Configuration conf,
			boolean isConfirmationNeeded

			) throws IOException {

		Collection<File> dirsToFormat = FSNamesystem.getNamespaceDirs(conf);
		Collection<File> editDirsToFormat =
				FSNamesystem.getNamespaceEditsDirs(conf);

		for(Iterator<File> it = dirsToFormat.iterator(); it.hasNext();) {
			File curDir = it.next();
			if (!curDir.exists())
				continue;

			if (isConfirmationNeeded) {
				System.err.print("Re-format filesystem in " + curDir +" ? (Y or N) ");

				if (!(System.in.read() == 'Y')) {
					System.err.println("Format aborted in "+ curDir);
					return true;
				}
				// discard the enter-key
				while(System.in.read() != '\n');
			}
		}

		FSNamesystem nsys = new FSNamesystem(new FSImage(dirsToFormat,
				editDirsToFormat), conf);

		nsys.dir.fsImage.format();
		return false;
	}

	private static boolean finalize(Configuration conf,

			boolean isConfirmationNeeded

			) throws IOException {

		Collection<File> dirsToFormat = FSNamesystem.getNamespaceDirs(conf);
		Collection<File> editDirsToFormat =

				FSNamesystem.getNamespaceEditsDirs(conf);
		FSNamesystem nsys = new FSNamesystem(new FSImage(dirsToFormat,
				editDirsToFormat), conf);

		System.err.print(
				"\"finalize\" will remove the previous state of the files system.\n"
						+ "Recent upgrade will become permanent.\n"
						+ "Rollback option will not be available anymore.\n");

		if (isConfirmationNeeded) {
			System.err.print("Finalize filesystem state ? (Y or N) ");

			if (!(System.in.read() == 'Y')) {
				System.err.println("Finalize aborted.");
				return true;
			}
			// discard the enter-key
			while(System.in.read() != '\n');
		}

		nsys.dir.fsImage.finalizeUpgrade();
		return false;
	}

	public static ContinuousNameNode createContinuousNameNode(String argv[],
			Configuration conf) throws IOException {

		if (conf == null)
			conf = new Configuration();

		StartupOption startOpt = parseArguments(argv);

		if (startOpt == null) {
			printUsage();
			return null;
		}

		setStartupOption(conf, startOpt);

		switch (startOpt) {

		case FORMAT:
			boolean aborted = format(conf, true);
			System.exit(aborted ? 1 : 0);

		case FINALIZE:
			aborted = finalize(conf, true);
			System.exit(aborted ? 1 : 0);

		default:

		}

		DefaultMetricsSystem.initialize("NameNode");

		ContinuousNameNode namenode = new ContinuousNameNode(conf);

		return namenode;

	}

	/*************************OASIS-INRIA******************************/

	/**
	 * The main which is called.
	 * @param argv
	 * @throws Exception
	 */
	public static void main(String argv[]) throws Exception {

		try {
			StringUtils.startupShutdownMessage(NameNode.class, argv, LOG);
			ContinuousNameNode namenode = createContinuousNameNode(argv, null);

			if (namenode != null) {

				/*************************OASIS-INRIA******************************/

				continuousClient =
						(ContinuousJobTrackerProtocol)RPC.waitForProxy(ContinuousJobTrackerProtocol.class,
								ContinuousJobTrackerProtocol.versionID,
								ContinuousUtil.getContinuousJobTrackerServerAddress(new Configuration()),
								new Configuration());

				System.out.println("Client Protocol version: " + continuousClient.getProtocolVersion());
				System.out.println("Client Protocol version: " + continuousClient.versionID);

				/*************************OASIS-INRIA******************************/

				namenode.join();

			}

		} catch (Throwable e) {
			LOG.error(StringUtils.stringifyException(e));
			System.exit(-1);
		}
	}

	@Override
	public long getProtocolVersion(String protocol, 
			long clientVersion) throws IOException {
		if (protocol.equals(ClientProtocol.class.getName())) {
			return ClientProtocol.versionID; 
		} else if (protocol.equals(DatanodeProtocol.class.getName())){
			return DatanodeProtocol.versionID;
		} else if (protocol.equals(NamenodeProtocol.class.getName())){
			return NamenodeProtocol.versionID;
		} else if (protocol.equals(RefreshAuthorizationPolicyProtocol.class.getName())){
			return RefreshAuthorizationPolicyProtocol.versionID;
		} else if (protocol.equals(RefreshUserMappingsProtocol.class.getName())){
			return RefreshUserMappingsProtocol.versionID;
		}
			else if (protocol.equals(TimeStampProtocol.class.getName())){
				return TimeStampProtocol.versionID;
		} else {
			throw new IOException("Unknown protocol to name node: " + protocol);
		}
	}
}
