package org.apache.hadoop.hdfs.server.namenode;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.ipc.VersionedProtocol;

public interface TimeStampProtocol extends VersionedProtocol {
	
	
	/**
	 * The TimeStampProtocol version.
	 */
	public static final long  versionID = 1337;
	
	public MapWritable getListOfTimeStampForDir(String pDir);
	
}
