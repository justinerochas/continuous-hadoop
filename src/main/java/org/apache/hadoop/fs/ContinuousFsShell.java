package org.apache.hadoop.fs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.shell.Count;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.RemoteException;
import org.apache.hadoop.util.ToolRunner;

public class ContinuousFsShell extends FsShell {

	public ContinuousFsShell()
	{
		super();
	}

	public  ContinuousFsShell(Configuration conf)
	{
		super(conf);
	}

	/**
	 * Redefine the method to use ContinuousFileUtil in place of FileUtil in the method code.
	 * {@inheritDoc}
	 * @param srcs
	 * @param dstf
	 * @throws IOException
	 */
	@Override
	public void copyFromLocal(Path[] srcs, String dstf) throws IOException {
		Path dstPath = new Path(dstf);
		FileSystem dstFs = dstPath.getFileSystem(getConf());


		if (srcs.length == 1 && srcs[0].toString().equals("-"))
			this.copyFromStdin(dstPath, dstFs);
		else{
			/*************************OASIS-INRIA******************************/
			// WE SHOULD SEE THIS ALL TO COMPARE WITH THE PREVIOUS METH
			Configuration conf = dstFs.getConf();
			ContinuousFileUtil.copy(FileSystem.getLocal(conf), srcs, dstFs, dstPath, false, true, conf);
		}
	}

	/**
	 * In this method we use ContinuousIOUtils in place of IOUtils.
	 * {@inheritDoc}
	 * @param dst
	 * @param dstFs
	 * @throws IOException
	 */
	private void copyFromStdin(Path dst, FileSystem dstFs) throws IOException {
		if (dstFs.isDirectory(dst)) {
			throw new IOException("When source is stdin, destination must be a file.");
		}
		if (dstFs.exists(dst)) {
			throw new IOException("Target " + dst.toString() + " already exists.");
		}
		FSDataOutputStream out = dstFs.create(dst);
		try {
			IOUtils.copyBytes(System.in, out, getConf(), false);
		}
		finally {
			out.close();
		}
	}


	/**
	 * Redefine the run method tu add job resubmission when the -Continuous parameter is the last parameter of the command line args.
	 */
	@Override
	public int run(String argv[]) throws Exception {
		if (argv.length < 1) {
			printUsage("");
			return -1;
		}

		int exitCode = -1;
		int i = 0;
		String cmd = argv[i++];

		//
		// verify that we have enough command line parameters
		//
		if ("-put".equals(cmd) || "-test".equals(cmd) ||
				"-copyFromLocal".equals(cmd) || "-moveFromLocal".equals(cmd)) {
			if (argv.length < 3) {
				printUsage(cmd);
				return exitCode;
			}
		} else if ("-get".equals(cmd) ||
				"-copyToLocal".equals(cmd) || "-moveToLocal".equals(cmd)) {
			if (argv.length < 3) {
				printUsage(cmd);
				return exitCode;
			}
		} else if ("-mv".equals(cmd) || "-cp".equals(cmd)) {
			if (argv.length < 3) {
				printUsage(cmd);
				return exitCode;
			}
		} else if ("-rm".equals(cmd) || "-rmr".equals(cmd) ||
				"-cat".equals(cmd) || "-mkdir".equals(cmd) ||
				"-touchz".equals(cmd) || "-stat".equals(cmd) ||
				"-text".equals(cmd)) {
			if (argv.length < 2) {
				printUsage(cmd);
				return exitCode;
			}
		}

		// initialize FsShell
		try {
			init();
		} catch (RPC.VersionMismatch v) {
			System.err.println("Version Mismatch between client and server" +
					"... command aborted.");
			return exitCode;
		} catch (IOException e) {
			System.err.println("Bad connection to FS. command aborted. exception: " +
					e.getLocalizedMessage());
			return exitCode;
		}

		exitCode = 0;
		try
		{
			if ("-put".equals(cmd) || "-copyFromLocal".equals(cmd)) {

				/*************************OASIS-INRIA******************************/
				String continuous = argv[argv.length-1];

				if("-continuous".equals(continuous)){
					System.out.println("OASIS-INRIA: continuous parameter is: " + continuous);
					Path[] srcs = new Path[argv.length-3];
					for (int j=0 ; i < argv.length-2 ;) {
						srcs[j++] = new Path(argv[i++]);
					}
					this.copyFromLocal(srcs, argv[i++]);
					/*************************OASIS-INRIA******************************/

				}else {
					Path[] srcs = new Path[argv.length-2];
					for (int j=0 ; i < argv.length-1 ;)
						srcs[j++] = new Path(argv[i++]);
					super.copyFromLocal(srcs, argv[i++]);
				}

			}
			// In the case that it is not a put or a -copyFromLocal we let the super method do what
			// it need
			else super.run(argv);
		}
		catch (IllegalArgumentException arge) {
			exitCode = -1;
			System.err.println(cmd.substring(1) + ": " + arge.getLocalizedMessage());
			printUsage(cmd);
		} catch (RemoteException e) {
			//
			// This is a error returned by hadoop server. Print
			// out the first line of the error mesage, ignore the stack trace.
			exitCode = -1;
			try {
				String[] content;
				content = e.getLocalizedMessage().split("\n");
				System.err.println(cmd.substring(1) + ": " +
						content[0]);
			} catch (Exception ex) {
				System.err.println(cmd.substring(1) + ": " +
						ex.getLocalizedMessage());
			}
		} catch (IOException e) {
			//
			// IO exception encountered locally.
			//
			exitCode = -1;
			System.err.println(cmd.substring(1) + ": " +
					e.getLocalizedMessage());
		} catch (Exception re) {
			exitCode = -1;
			System.err.println(cmd.substring(1) + ": " + re.getLocalizedMessage());
		} finally {
		}
		return exitCode;
	}

	/**
	 * Define the printUsage private method.
	 * @param cmd
	 */
	private static void printUsage(String cmd) {
		String prefix = "Usage: java " + FsShell.class.getSimpleName();
		if ("-fs".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [-fs <local | file system URI>]");
		} else if ("-conf".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [-conf <configuration file>]");
		} else if ("-D".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [-D <[property=value>]");
		} else if ("-ls".equals(cmd) || "-lsr".equals(cmd) ||
				"-du".equals(cmd) || "-dus".equals(cmd) ||
				"-touchz".equals(cmd) || "-mkdir".equals(cmd) ||
				"-text".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [" + cmd + " <path>]");
		} else if (Count.matches(cmd)) {
			System.err.println(prefix + " [" + Count.USAGE + "]");
		} else if ("-rm".equals(cmd) || "-rmr".equals(cmd)) {
			System.err.println("Usage: java FsShell [" + cmd +
					" [-skipTrash] <src>]");
		} else if ("-mv".equals(cmd) || "-cp".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [" + cmd + " <src> <dst>]");
		} else if ("-put".equals(cmd) || "-copyFromLocal".equals(cmd) ||
				"-moveFromLocal".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [" + cmd + " <localsrc> ... <dst>]");
		} else if ("-get".equals(cmd)) {
			System.err.println("Usage: java FsShell [" + GET_SHORT_USAGE + "]");
		} else if ("-copyToLocal".equals(cmd)) {
			System.err.println("Usage: java FsShell [" + COPYTOLOCAL_SHORT_USAGE+ "]");
		} else if ("-moveToLocal".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [" + cmd + " [-crc] <src> <localdst>]");
		} else if ("-cat".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [" + cmd + " <src>]");
		} else if ("-setrep".equals(cmd)) {
			System.err.println("Usage: java FsShell [" + SETREP_SHORT_USAGE + "]");
		} else if ("-test".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [-test -[ezd] <path>]");
		} else if ("-stat".equals(cmd)) {
			System.err.println("Usage: java FsShell" +
					" [-stat [format] <path>]");
		} else if ("-tail".equals(cmd)) {
			System.err.println("Usage: java FsShell [" + TAIL_USAGE + "]");
		} else {
			System.err.println("Usage: java FsShell");
			System.err.println("           [-ls <path>]");
			System.err.println("           [-lsr <path>]");
			System.err.println("           [-du <path>]");
			System.err.println("           [-dus <path>]");
			System.err.println("           [" + Count.USAGE + "]");
			System.err.println("           [-mv <src> <dst>]");
			System.err.println("           [-cp <src> <dst>]");
			System.err.println("           [-rm [-skipTrash] <path>]");
			System.err.println("           [-rmr [-skipTrash] <path>]");
			System.err.println("           [-expunge]");
			System.err.println("           [-put <localsrc> ... <dst>]");
			System.err.println("           [-copyFromLocal <localsrc> ... <dst>]");
			System.err.println("           [-moveFromLocal <localsrc> ... <dst>]");
			System.err.println("           [" + GET_SHORT_USAGE + "]");
			System.err.println("           [-getmerge <src> <localdst> [addnl]]");
			System.err.println("           [-cat <src>]");
			System.err.println("           [-text <src>]");
			System.err.println("           [" + COPYTOLOCAL_SHORT_USAGE + "]");
			System.err.println("           [-moveToLocal [-crc] <src> <localdst>]");
			System.err.println("           [-mkdir <path>]");
			System.err.println("           [" + SETREP_SHORT_USAGE + "]");
			System.err.println("           [-touchz <path>]");
			System.err.println("           [-test -[ezd] <path>]");
			System.err.println("           [-stat [format] <path>]");
			System.err.println("           [" + TAIL_USAGE + "]");
			System.err.println("           [" + FsShellPermissions.CHMOD_USAGE + "]");
			System.err.println("           [" + FsShellPermissions.CHOWN_USAGE + "]");
			System.err.println("           [" + FsShellPermissions.CHGRP_USAGE + "]");
			System.err.println("           [-help [cmd]]");
			System.err.println();
			ToolRunner.printGenericCommandUsage(System.err);
		}
	}

	/**
	 * main() has some simple utility methods
	 */
	public static void main(String argv[]) throws Exception {
		ContinuousFsShell shell = new ContinuousFsShell();
		int res;
		try {
			res = ToolRunner.run(shell, argv);
		} finally {
			shell.close();
		}
		System.exit(res);
	}
}
