package org.apache.hadoop.fs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IOUtils;

import fr.inria.oasis.hadoop.continuous.config.EditTextFile;

/**
 * Provides a continuous implementation of some method into the FileUtil class.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public class ContinuousFileUtil extends FileUtil {

	/**
	 * {@inheritDoc}
	 * @param srcName
	 * @param dstFS
	 * @param dst
	 * @param overwrite
	 * @return
	 * @throws IOException
	 */
	private static Path checkDest(String srcName, FileSystem dstFS, Path dst,
			boolean overwrite) throws IOException {
		if (dstFS.exists(dst)) {
			FileStatus sdst = dstFS.getFileStatus(dst);
			if (sdst.isDir()) {
				if (null == srcName) {
					throw new IOException("Target " + dst + " is a directory");
				}
				return checkDest(null, dstFS, new Path(dst, srcName), overwrite);
			} else if (!overwrite) {
				throw new IOException("Target " + dst + " already exists");
			}
		}
		return dst;
	}

	/**
	 * {@inheritDoc}
	 * @param srcFS
	 * @param src
	 * @param dstFS
	 * @param dst
	 * @throws IOException
	 */
	private static void checkDependencies(FileSystem srcFS,
			Path src,
			FileSystem dstFS,
			Path dst)
					throws IOException {
		if (srcFS == dstFS) {
			String srcq = src.makeQualified(srcFS).toString() + Path.SEPARATOR;
			String dstq = dst.makeQualified(dstFS).toString() + Path.SEPARATOR;
			if (dstq.startsWith(srcq)) {
				if (srcq.length() == dstq.length()) {
					throw new IOException("Cannot copy " + src + " to itself.");
				} else {
					throw new IOException("Cannot copy " + src + " to its subdirectory " +
							dst);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @param srcFS
	 * @param srcs
	 * @param dstFS
	 * @param dst
	 * @param deleteSource
	 * @param overwrite
	 * @param conf
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(FileSystem srcFS, Path[] srcs,
			FileSystem dstFS, Path dst,
			boolean deleteSource,
			boolean overwrite, Configuration conf)
					throws IOException {
		boolean gotException = false;
		boolean returnVal = true;
		StringBuilder exceptions = new StringBuilder();

		if (srcs.length == 1)
			return copy(srcFS, srcs[0], dstFS, dst, deleteSource, overwrite, conf);

		// Check if dest is directory
		if (!dstFS.exists(dst)) {
			throw new IOException("`" + dst +"': specified destination directory " +
					"doest not exist");
		} else {
			FileStatus sdst = dstFS.getFileStatus(dst);
			if (!sdst.isDir())
				throw new IOException("copying multiple files, but last argument `" +
						dst + "' is not a directory");
		}

		for (Path src : srcs) {
			try {
				if (!copy(srcFS, src, dstFS, dst, deleteSource, overwrite, conf))
					returnVal = false;
			} catch (IOException e) {
				gotException = true;
				exceptions.append(e.getMessage());
				exceptions.append("\n");
			}
		}
		if (gotException) {
			throw new IOException(exceptions.toString());
		}
		return returnVal;
	}

	/**
	 * {@inheritDoc}
	 * @param srcFS
	 * @param src
	 * @param dstFS
	 * @param dst
	 * @param deleteSource
	 * @param overwrite
	 * @param conf
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(FileSystem srcFS, Path src,
			FileSystem dstFS, Path dst,
			boolean deleteSource,
			boolean overwrite,
			Configuration conf) throws IOException {
		FileStatus fileStatus = srcFS.getFileStatus(src);
		return copy(srcFS, fileStatus, dstFS, dst, deleteSource, overwrite, conf);
	}

	/**
	 * {@inheritDoc}
	 * @param srcFS
	 * @param srcStatus
	 * @param dstFS
	 * @param dst
	 * @param deleteSource
	 * @param overwrite
	 * @param conf
	 * @return
	 * @throws IOException
	 */
	private static boolean copy(FileSystem srcFS, FileStatus srcStatus,
			FileSystem dstFS, Path dst,
			boolean deleteSource,
			boolean overwrite,
			Configuration conf) throws IOException {

		Path src = srcStatus.getPath();
		dst = checkDest(src.getName(), dstFS, dst, overwrite);
		if (srcStatus.isDir()) {
			checkDependencies(srcFS, src, dstFS, dst);
			if (!dstFS.mkdirs(dst)) {
				return false;
			}
			FileStatus contents[] = srcFS.listStatus(src);
			for (int i = 0; i < contents.length; i++) {
				copy(srcFS, contents[i], dstFS,
						new Path(dst, contents[i].getPath().getName()),
						deleteSource, overwrite, conf);
			}
		} else {
			InputStream in=null;
			OutputStream out = null;
			try {

				in = srcFS.open(src);
				out = dstFS.create(dst, overwrite);
				
				//ContinuousInputStream tCIS = new ContinuousInputStream(in);
				IOUtils.copyBytes(in,out,conf);

			} catch (IOException e) {
				IOUtils.closeStream(out);
				IOUtils.closeStream(in);
				throw e;
			}
		}

		if (deleteSource) {
			return srcFS.delete(src, true);
		} else {
			return true;
		}
	}
}
