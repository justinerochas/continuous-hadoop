package org.apache.hadoop.fs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.io.IOUtils;

import com.sun.corba.se.impl.ior.WireObjectKeyTemplate;

import fr.inria.oasis.hadoop.continuous.config.EditTextFile;

/**
 * This class provides a way to timestamp data.
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clément Agarini
 *
 */
public class ContinuousInputStream extends InputStream {

	/**
	 * BufferedReader in where data are read.
	 */
	BufferedReader br; //; = new BufferedReader(new InputStreamReader(in));
	
	/**
	 * If we the line read is larger than the buffersize we store data and we write them on the next call of read.
	 */
	Queue<Byte> byteSaved;

	/**
	 * We create the bufferedReader from the inputstream in parameter.
	 * @param pInputStream
	 */
	public ContinuousInputStream(InputStream pInputStream )
	{
		this.br = new BufferedReader(new InputStreamReader(pInputStream));
		byteSaved = new LinkedList<Byte>();
	}

	/**
	 * Fill the byte[] in param. Get line from buffered reader, timestamp them and them store them into b.
	 */
	@Override
	public int read(byte[] b) throws IOException {

		//First if there is saved byte we write saved byte to the buffer
		int end = (byteSaved.size()>b.length)?b.length:byteSaved.size();
		int writtenBytes = 0;
		
		for(int i =0; i < end; i++)
		{
			b[i] = byteSaved.poll();
		}
		
		writtenBytes += end;

		if(end < b.length)
		{

			String strLine = null;
			while(((strLine = br.readLine()) !=null)&& writtenBytes < b.length)
			{
				EditTextFile editText = new EditTextFile(strLine, null);
				editText.modify();
				byte[] bToWrite = editText.getModifiledTimeStr().getBytes();

				//Compute where we have to stop write
				int newend = ((bToWrite.length + writtenBytes )>b.length)?b.length-writtenBytes:bToWrite.length;
				
				// we restart where the byte saved have been written
				for(int i=0; i < newend;i++)
				{
					b[writtenBytes]=bToWrite[i];
					writtenBytes++;
				}

				
				// We save the byte that haven't been written
				for(int i = newend; i < bToWrite.length; i ++)
				{
					byteSaved.add(bToWrite[i]);
				}
				
				//writtenBytes+=bToWrite.length;
				
			}
			

			if(writtenBytes < b.length)
			{
				if(writtenBytes> 0 )
					return writtenBytes;
				else return -1;
			}
			else return b.length;

		}
		
		return end;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		byte[] b = new byte[1];
		return this.read(b);
	}

}
