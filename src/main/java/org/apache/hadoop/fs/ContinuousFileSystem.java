package org.apache.hadoop.fs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;

public abstract class ContinuousFileSystem extends FileSystem {

	/*************************OASIS-INRIA******************************/
	@Override
	public void copyFromLocalFile(boolean delSrc, boolean overwrite,
			Path[] srcs, Path dst)
					throws IOException {
		Configuration conf = getConf();
		ContinuousFileUtil.copy(getLocal(conf), srcs, this, dst, delSrc, overwrite, conf);
	}
	/*************************OASIS-INRIA******************************/

}
