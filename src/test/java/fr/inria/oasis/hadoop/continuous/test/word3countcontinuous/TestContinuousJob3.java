package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * Test class to copy & read a single file inside HDFS using eon cluster. Then
 *
 * it writes a second file. It is using a continuous job + carry with JUnit 4
 *
 * tests.
 *
 * @author adamiano
 *
 *
 */

public class TestContinuousJob3 {
	// The host where to run the jobs
	private static String host;

	// The log
	static private Log LOG = LogFactory.getLog(TestContinuousJob3.class);

	// Test text files
	public static String pathToTextFiles ;
	public static String textFileName;
	public static String textFileName2;

	// Conf
	private static Configuration conf;

	// FS
	private static FileSystem fs;

	// Paths
	private static String ROOT_DIR;
	private static String carryFolder;

	@Before
	public void setUp() throws Exception {
		//Load the properties
		Properties prop = new Properties();

		FileInputStream pathToProperties =  new FileInputStream("./src/fr/inria/oasis/hadoop/continuous/test/word3countcontinuous/test.properties");
		prop.load(pathToProperties);
		pathToProperties.close();

		TestContinuousJob3.host =  prop.getProperty("host");
		TestContinuousJob3.ROOT_DIR = prop.getProperty("ROOT_DIR");
		TestContinuousJob3.pathToTextFiles=prop.getProperty("pathToTextFiles");
		TestContinuousJob3.textFileName=prop.getProperty("textFileName");
		TestContinuousJob3.textFileName2=prop.getProperty("textFileName2");
		TestContinuousJob3.carryFolder = TestContinuousJob3.ROOT_DIR + prop.getProperty("carryFolder");


		//Create the configuration
		conf = new Configuration();
		// Set the config to run on other machine/cluster
		conf.set("fs.default.name", "hdfs://" + host + ":9000");
		conf.set("mapred.job.tracker", host + ":9001");
		conf.set("mapred.continuous.manager.control.host", host);

		// Set the test jar
		conf.set("mapred.jar", "./build/classes/Word3countContinuous.jar");

		// Used to read/write files on HDFS.
		fs = FileSystem.get(conf);

	}

	@Test
	public void TestReadCarryFile1() throws ClassNotFoundException, IOException, InterruptedException {
		// READ the CARRY file

		try {
			LOG.info("- PRAD - INRIA - : Reading first carry file.");
			FileStatus[] status = fs.listStatus(new Path(carryFolder));
			Boolean hasReadTheFile = false;

			if (status != null) {
				for (int i = 0; i < status.length; i++) {
					if (status[i].getPath().toString().contains("carry")) {
						// Open & read the file
						BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
						String readedLine = br.readLine();
						int nbLineReaded = 0;

						while (readedLine != null) {
							hasReadTheFile = true;
							nbLineReaded++;
							LOG.info("***** Line read : " + readedLine);

							// For each line, test if the timestamp has been
							// added correctly.

							switch (nbLineReaded) {
							case 1:
								assertEquals("titi\t1", readedLine);
								break;

							case 2:
								assertEquals("toto\t2", readedLine);
								break;
							}

							readedLine = br.readLine();
						}

						// Close the file
						br.close();
					}

				}

			}

			else
			{
				fail("Error creating or reading the first carry file on HDFS");
			}

			if (hasReadTheFile != true) {
				fail("Error creating or reading the first carry file on HDFS");
			}

		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
			fail("Error creating or reading the first carry file on HDFS");
		}
	}

}
