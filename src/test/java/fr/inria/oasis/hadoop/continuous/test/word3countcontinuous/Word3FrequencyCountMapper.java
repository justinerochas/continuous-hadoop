package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import fr.inria.oasis.hadoop.continuous.mapreduce.ContinuousMapper;

public class Word3FrequencyCountMapper extends ContinuousMapper<Text, Text, Text, IntWritable> {

	protected static Logger logger = Logger.getLogger(Word3FrequencyCountMapper.class) ;
	private final static IntWritable one = new IntWritable(1);
	private Text word = new Text();

	public void map(Text key, Text value, Context context
			) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString());
		logger.info(" - PRAD - INRIA - : Parsing input") ;

		while (itr.hasMoreTokens()) {
			word.set(itr.nextToken());
			logger.info(" - PRAD - INRIA - : Generating sample [key=" + word + " value=" + one + "]") ;
			context.write(word, one);
		}
	}

}
