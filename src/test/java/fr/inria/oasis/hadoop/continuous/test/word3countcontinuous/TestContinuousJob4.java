package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.ContinuousFsShell;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * Test class to copy & read a single file inside HDFS using eon cluster. Then
 *
 * it writes a second file. It is using a continuous job + carry with JUnit 4
 *
 * tests.
 *
 *
 *
 * @author adamiano
 *
 *
 */

public class TestContinuousJob4 {
	// The host where to run the jobs
	private static String host;

	// The log
	static private Log LOG = LogFactory.getLog(TestContinuousJob4.class);

	// Test text files
	public static String pathToTextFiles ;
	public static String textFileName;
	public static String textFileName2;

	// Conf
	private static Configuration conf;

	// FS
	private static FileSystem fs;
	private static ContinuousFsShell shell;

	// Paths
	private static String ROOT_DIR;
	private static String inputFolder;
	private static String outputFolder;
	private static String carryFolder;
	private static Path inputPathTextFileName2;
	@Before
	public void setUp() throws Exception {
		//Load the properties
		Properties prop = new Properties();

		FileInputStream pathToProperties =  new FileInputStream("./src/fr/inria/oasis/hadoop/continuous/test/word3countcontinuous/test.properties");
		prop.load(pathToProperties);
		pathToProperties.close();

		TestContinuousJob4.host =  prop.getProperty("host");
		TestContinuousJob4.ROOT_DIR = prop.getProperty("ROOT_DIR");
		TestContinuousJob4.pathToTextFiles=prop.getProperty("pathToTextFiles");
		TestContinuousJob4.textFileName=prop.getProperty("textFileName");
		TestContinuousJob4.textFileName2=prop.getProperty("textFileName2");
		TestContinuousJob4.inputFolder = TestContinuousJob4.ROOT_DIR + prop.getProperty("inputFolder");
		TestContinuousJob4.outputFolder = TestContinuousJob4.ROOT_DIR + prop.getProperty("outputFolder");
		TestContinuousJob4.carryFolder = TestContinuousJob4.ROOT_DIR + prop.getProperty("carryFolder");
		TestContinuousJob4.inputPathTextFileName2 = new Path(inputFolder + textFileName2);

		//Create the configuration
		conf = new Configuration();
		// Set the config to run on other machine/cluster
		conf.set("fs.default.name", "hdfs://" + host + ":9000");
		conf.set("mapred.job.tracker", host + ":9001");
		conf.set("mapred.continuous.manager.control.host", host);

		// Set the test jar
		conf.set("mapred.jar", "./build/classes/Word3countContinuous.jar");

		// Used to read/write files on HDFS.
		fs = FileSystem.get(conf);
		shell = new ContinuousFsShell(conf);

	}

	@Test
	public void TestCreateSecondContinuousJob() throws ClassNotFoundException, IOException, InterruptedException {

		// Continous part, adding appened text file.
		try {
			LOG.info("- PRAD - INRIA - : Creating the second input test file.");
			// Delete the file if it already exists
			if (fs.exists(inputPathTextFileName2))
				fs.delete(inputPathTextFileName2, false);

			Path[] paths = { new Path(pathToTextFiles + textFileName2) };
			shell.copyFromLocal(paths, inputPathTextFileName2.toString());
			LOG.info("- PRAD - INRIA - : Second input test file created.");
		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
			fail("Error creating the secondary input file on HDFS");
		}

		System.out.println("sleep - wait for the second new job to be submitted");


		try {
			TestContinuousJob1.job.waitForResubmitionCompletion();

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		LOG.info("- PRAD - INRIA - : The **NEW** job has been completed.");

		// READ the OUTPUT file
		try {
			// Read the output file
			LOG.info("- PRAD - INRIA - : Reading the output for the second test file.");
			Boolean hasReadTheFile = false;

			// For each "continuous_xxx" folder inside continuous-output folder
			FileStatus[] folderStatus = fs.listStatus(new Path(outputFolder));

			if (folderStatus != null) {
				ArrayList<Long> listOfDates = new ArrayList<Long>();
				// For each "continuous_xxx" folder inside output folder
				for (int i = 0; i < folderStatus.length; i++) {
					if (folderStatus[i].getPath().toString().contains("continuous-output")) {
						System.out.println("Found a continuous-output folder");

						// hdfs://eon3.inria.fr:9000/user/adamiano/tmp/test-output/continuous-output_20120521145300
						//Add the date of the folder to the list
						if (folderStatus != null) {
							listOfDates.add(folderStatus[i].getModificationTime());
						}

						//Sort the list
						Collections.sort(listOfDates);
					}
				}
				//read all the folder on the output dir
				FileStatus[] statusFolderbis = fs.listStatus(new Path(outputFolder));

				//But only read inside the most recently modified (last job result)
				for (int k = 0; k < statusFolderbis.length; k++) {
					if(statusFolderbis[k].getModificationTime() == (long)listOfDates.get(listOfDates.size()-1)){
						System.out.println("Found the last continuous-output folder");

						FileStatus[] status = fs.listStatus(statusFolderbis[k].getPath());
						//For each part-r-xxx
						for (int j = 0; j < status.length; j++) {
							if (status[j].getPath().toString().contains("part")) {

								// Open the file
								BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status[j].getPath())));
								String readedLine = br.readLine();
								int nbLineReaded = 0;

								while (readedLine != null) {
									hasReadTheFile = true;
									nbLineReaded++;
									LOG.info("- PRAD - INRIA - READ : " + readedLine);

									switch (nbLineReaded) {

									case 1:
										assertEquals("toto\t3", readedLine);
										break;

									default:
										break;

									}
									readedLine = br.readLine();

								}
								// Close the file
								br.close();
							}
						}
					}
				}
			}

			else {
				fail("Error creating or reading the second output file on HDFS");
			}

			if (hasReadTheFile != true) {
				fail("Error reading the second  output file on HDFS");
			}

		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
			fail("Error creating or reading the second output file on HDFS");

		}

		// READ the CARRY file
		try {
			LOG.info("- PRAD - INRIA - : Reading second carry file.");
			FileStatus[] status = fs.listStatus(new Path(carryFolder));
			Boolean hasReadTheFile = false;

			if (status != null) {
				for (int i = 0; i < status.length; i++) {
					if (status[i].getPath().toString().contains("carry")) {
						// Open & read the file
						BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
						String readedLine = br.readLine();
						int nbLineReaded = 0;

						while (readedLine != null) {
							hasReadTheFile = true;
							nbLineReaded++;

							LOG.info("***** Line read : " + readedLine);

							// For each line, test if the timestamp has been
							// added correctly.

							switch (nbLineReaded) {
							case 1:
								assertEquals("tata\t1", readedLine);
								break;

							case 2:
								assertEquals("titi\t1", readedLine);
								break;

							}

							readedLine = br.readLine();
						}
						// Close the file
						br.close();
					}
				}
			}

			else
			{
				fail("Error creating or reading the first carry file on HDFS");

			}

			if (hasReadTheFile != true) {
				fail("Error reading the first carry file on HDFS");
			}
		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
			fail("Error creating or reading the first carry file on HDFS");

		}
	}

}
