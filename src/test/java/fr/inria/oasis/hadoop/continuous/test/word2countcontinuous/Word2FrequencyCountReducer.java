package fr.inria.oasis.hadoop.continuous.test.word2countcontinuous;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import fr.inria.oasis.hadoop.continuous.mapreduce.ContinuousReducer;

/**
 * Represents the reducer class for the Word2FrequencyCount job.
 * This class must extends ContinuousReducer since the job is continuous.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clement Agarini
 */
public class Word2FrequencyCountReducer extends ContinuousReducer<Text,IntWritable,Text,IntWritable> {

	protected static Logger logger = Logger.getLogger(Word2FrequencyCountReducer.class) ;
	private IntWritable result = new IntWritable();

	/**
	 * Determines what to do when values of each key are reduced.
	 * This is where you can decide if the key and the values reduced are to be carried or to be output.
	 * Call context.write to write the key and the value computed in the definitive output file.
	 * Call context.carry to write the key and the value computed in the carry file. These data will be loaded for the next job.
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void continuousReduce(Text key, Iterable<IntWritable> values, ContinuousContext context) throws IOException, InterruptedException {

		logger.info(" - PRAD - INRIA - : Reducing key " + key) ;

		int sum = 0;

		for(IntWritable i : values) {

			sum = sum + i.get();
		}

		logger.info(" - PRAD - INRIA - : Value computed is " + sum) ;
		result.set(sum) ;

		if(sum < 2) {

			logger.info(" - PRAD - INRIA - : Key and value are carried") ;
			context.carry(key, result);
		}

		else {

			logger.info(" - PRAD - INRIA - : Key and value are outputed") ;
			context.write(key, result);
		}
	}


}
