package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.ContinuousFsShell;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.junit.Before;
import org.junit.Test;

import fr.inria.oasis.hadoop.continuous.config.ContinuousJobConfig;
import fr.inria.oasis.hadoop.continuous.job.ContinuousJob;

/**
 *
 * Test class to copy & read a single file inside HDFS using eon cluster. Then
 *
 * it writes a second file. It is using a continuous job + carry with JUnit 4
 *
 * tests.
 *
 *
 *
 * @author adamiano
 *
 *
 */

public class TestContinuousJob1 {
	// The host where to run the jobs
	private static String host;

	// The log
	static private Log LOG = LogFactory.getLog(TestContinuousJob1.class);

	// Test text files
	public static String pathToTextFiles ;
	public static String textFileName;
	public static String textFileName2;

	// Conf
	private static Configuration conf;

	// FS
	private static FileSystem fs;
	private static ContinuousFsShell shell;

	// The continuous job
	public static ContinuousJob job;

	// Paths
	private static String ROOT_DIR;
	private static String inputFolder;
	private static String outputFolder;
	private static String carryFolder;
	private static Path inputPathTextFileName1;
	private static String DateNowFormatted;


	@Before
	public void setUp() throws Exception {
		//Load the properties
		Properties prop = new Properties();

		FileInputStream pathToProperties =  new FileInputStream("./src/fr/inria/oasis/hadoop/continuous/test/word3countcontinuous/test.properties");
		prop.load(pathToProperties);
		pathToProperties.close();

		TestContinuousJob1.host =  prop.getProperty("host");
		TestContinuousJob1.ROOT_DIR = prop.getProperty("ROOT_DIR");
		TestContinuousJob1.pathToTextFiles=prop.getProperty("pathToTextFiles");
		TestContinuousJob1.textFileName=prop.getProperty("textFileName");
		TestContinuousJob1.textFileName2=prop.getProperty("textFileName2");
		TestContinuousJob1.inputFolder = TestContinuousJob1.ROOT_DIR + prop.getProperty("inputFolder");
		TestContinuousJob1.outputFolder = TestContinuousJob1.ROOT_DIR + prop.getProperty("outputFolder");
		TestContinuousJob1.carryFolder = TestContinuousJob1.ROOT_DIR + prop.getProperty("carryFolder");
		TestContinuousJob1.inputPathTextFileName1 = new Path(inputFolder + textFileName);


		//Create the configuration
		conf = new Configuration();
		// Set the config to run on other machine/cluster
		conf.set("fs.default.name", "hdfs://" + host + ":9000");
		conf.set("mapred.job.tracker", host + ":9001");
		conf.set("mapred.continuous.manager.control.host", host);

		// Set the test jar
		conf.set("mapred.jar", "./build/classes/Word3countContinuous.jar");


		// Used to read/write files on HDFS.
		fs = FileSystem.get(conf);
		shell = new ContinuousFsShell(conf);

		// Create the job
		job = new ContinuousJob(conf, "Word3CountContinuous",true);
		job.setJarByClass(TestContinuousJob1.class);
		job.setNumReduceTasks(1);

		// Delete input folder if it already exists
		String[] argsMkdir2 = { "-rmr", ROOT_DIR + "/tmp" };
		shell.run(argsMkdir2);

		// Create a new inputs folder
		fs.mkdirs(new Path(inputFolder));

		// Set mapreduce classes
		job.setMapperClass(Word3FrequencyCountMapper.class);
		job.setReducerClass(Word3FrequencyCountContinuousReducer.class);

		// input for mapper
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		//
		TextOutputFormat.setOutputPath(job, new Path(outputFolder));

		// add input folder as the input for this job
		FileInputFormat.addInputPath(job, new Path(inputFolder));
		// set output folder
		FileOutputFormat.setOutputPath(job, new Path(outputFolder));
		// set for carry on
		job.setCarryOnFolder(carryFolder);

		// Run the first job doing nothing to prepare continuous jobs
		LOG.info("- PRAD - INRIA - : Submitting the dummy job.");
		job.waitForCompletion(true);
		LOG.info("- PRAD - INRIA - : The dummy job has completed.");

	}

	@Test
	public void TestCreateContinuousFile1() throws ClassNotFoundException, IOException, InterruptedException {
		// Continous part, adding new text file.

		try {
			LOG.info("- PRAD - INRIA - : Creating test file.");

			// Delete the file if it already exists
			if (fs.exists(inputPathTextFileName1)) {
				fs.delete(inputPathTextFileName1, false);
			}

			// Set the path from the source text file to copy
			Path[] paths = { new Path(pathToTextFiles + textFileName) };

			// Copy the local file to the cluster
			shell.copyFromLocal(paths, inputPathTextFileName1.toString());
			LOG.info("- PRAD - INRIA - : Test file created.");

			// Read the input file
			LOG.info("- PRAD - INRIA - : Reading test file.");
			FileStatus[] status = fs.listStatus(new Path(inputFolder));
			Boolean hasReadTheFile = false;

			if (status != null) {
				for (int i = 0; i < status.length; i++) {
					if (status[i].getPath().toString().contains(textFileName)) {

						// Open & read the file
						BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
						String readedLine = br.readLine();
						int nbLineReaded = 0;
						while (readedLine != null) {
							nbLineReaded++;

							hasReadTheFile = true;

							LOG.info("- PRAD - INRIA - READ : " + readedLine);

							// For each line, test if the timestamp has been
							// added correctly.
							switch (nbLineReaded) {

							case 1:

								// ex : titi toto 2012-05-1513:50:31

								Date dateNow = new Date();
								DateFormat dateFormat = new SimpleDateFormat(ContinuousJobConfig.DATE_FORMAT);
								DateNowFormatted = dateFormat.format(dateNow);
								DateNowFormatted = DateNowFormatted.substring(0, DateNowFormatted.length() - 4);
								assertEquals("titi toto " + DateNowFormatted, readedLine.substring(0, readedLine.length() - 4));
								break;

							case 2:
								assertEquals("tutu tutu " + DateNowFormatted, readedLine.substring(0, readedLine.length() - 4));
								break;

							case 3:
								assertEquals("tutu toto " + DateNowFormatted, readedLine.substring(0, readedLine.length() - 4));
								break;
							}
							readedLine = br.readLine();
						}
						// Close the file
						br.close();
					}
				}
			}

			else {
				fail("Error creating or reading the input file on HDFS");
			}

			if (hasReadTheFile != true) {
				fail("Error reading the input file on HDFS");
			}
		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
			fail("Error creating or reading the input file on HDFS");
		}
	}

}
