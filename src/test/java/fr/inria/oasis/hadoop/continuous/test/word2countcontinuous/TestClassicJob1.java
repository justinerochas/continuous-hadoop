package fr.inria.oasis.hadoop.continuous.test.word2countcontinuous;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FsShell;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class to write & read a single file inside HDFS using eon cluster.
 *
 * @author adamiano
 *
 */
public class TestClassicJob1 {
	private final static String host = "eon3";

	private static String ROOT_DIR = "/user/adamiano";

	static private Log LOG = LogFactory.getLog(TestClassicJob1.class);
	// test text file
	public static final String theFilename = "test.txt";
	public static final String message = "titi toto";

	private static FileSystem fs;
	private static Configuration conf;
	private static Job job;

	@Before
	public void setUP() throws Exception {
		//Set the config to run on the cluster
		conf = new Configuration();
		// run on other machine/cluster
		conf.set("fs.default.name", "hdfs://" + host + ":9000");
		conf.set("mapred.job.tracker", host + ":9001");
		//Set the test jar
		conf.set("mapred.jar", "./build/classes/Word2CountContinuous.jar");
		fs = FileSystem.get(conf);


		job = new Job(conf,"Word2count");
		//job.setJobName( "Word2count");
		//job = new Job(conf, "Word2count");

		job.setJarByClass(TestClassicJob1.class);


		FsShell shell = new FsShell(conf);

		// Delete input folder if it already exists
		String[] argsMkdir2 = { "-rmr", ROOT_DIR + "/*" };
		shell.run(argsMkdir2);

		// Create a new inputs folder
		String[] argsMkdir = { "-mkdir", ROOT_DIR + "/tmp/test-input" };
		shell.run(argsMkdir);

		// add input folder as the input for this job
		FileInputFormat.setInputPaths(job, new Path(ROOT_DIR + "/tmp/test-input"));
		// set output folder
		FileOutputFormat.setOutputPath(job, new Path(ROOT_DIR + "/tmp/test-output"));



		// Set mapreduce classes
		job.setMapperClass(Word2FrequencyCountMapper.class);
		job.setReducerClass(Word2FrequencyCountReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

	}

	@Test
	public void TestCreateFile() throws ClassNotFoundException, IOException, InterruptedException {
		// Create & write some line inside a new text file
		try {
			// Delete the file if it already exists
			if (fs.exists(new Path(ROOT_DIR + "/tmp/test-input/" + theFilename)))
				fs.delete(new Path(ROOT_DIR + "/tmp/test-input/" + theFilename), false);

			// Copy the local file to the cluster
			fs.copyFromLocalFile(new Path("./src/fr/inria/oasis/hadoop/continuous/test/word2countcontinuous/" + theFilename), new Path(ROOT_DIR + "/tmp/test-input/" + theFilename));

		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
		}


		// Run the job
		job.waitForCompletion(true);


		try {
			// Read the output file
			FileStatus[] status = fs.listStatus(new Path(ROOT_DIR + "/tmp/test-output/"));

			for (int i = 0; i < status.length; i++) {
				if (status[i].getPath().toString().contains("part")) {
					BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));

					String readedLine = br.readLine();

					int nbLineReaded = 0;

					while (readedLine != null) {
						nbLineReaded++;
						LOG.info("***** Line read : " + readedLine);

						switch (nbLineReaded) {
						case 1:
							assertEquals("titi\t2", readedLine);
							break;
						case 2:
							assertEquals("toto\t2", readedLine);
							break;
						default:
							break;
						}

						readedLine = br.readLine();
					}

					assertFalse(nbLineReaded == 0);

					br.close();
				}
			}

		} catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
		}
	}

}
