package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(	value={
		TestContinuousJob1.class,
		TestContinuousJob2.class,
		TestContinuousJob3.class,
		TestContinuousJob4.class
})


public class AllTestsContinuous {

}
