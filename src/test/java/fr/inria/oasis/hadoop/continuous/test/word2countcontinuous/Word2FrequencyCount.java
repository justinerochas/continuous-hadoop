package fr.inria.oasis.hadoop.continuous.test.word2countcontinuous;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.Logger;

import fr.inria.oasis.hadoop.continuous.config.TextNumericCarryFormat;
import fr.inria.oasis.hadoop.continuous.job.ContinuousJob;

/**
 * Provides a test case for continuous job.
 * This outputs the words that appear at least twice in input files, and carries the words that appear less than twice.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clement Agarini
 */
public class Word2FrequencyCount {
	protected static Logger logger = Logger.getLogger(Word2FrequencyCountMapper.class) ;

	private Path input ;
	private Path output;
	private Path carry ;

	/**
	 * Constructs the job with the path of the folder where data to process are going to be putting,
	 * the path of the folder where successive jobs are going to be writting on
	 * and the path where carry folder is going to be.
	 *
	 * Paths specified are on HDFS.
	 *
	 * @param input
	 * @param output
	 * @param carry
	 */
	public Word2FrequencyCount(Path input, Path output, Path carry){

		this.input = input;
		this.output = output;
		this.carry = carry ;

		System.out.println("INPUT PATH :" + this.input + ", OUTPUT PATH : " + this.output + ", CARRY PATH : " + this.carry) ;
	}


	/**
	 * Sets the parameters of the job and runs it.
	 * Paremeters required for a continuous job are specified for the next bloc of instructions.
	 *
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public void run() throws IOException, InterruptedException, ClassNotFoundException{

		ContinuousJob job = new ContinuousJob() ;

		job.setJobName("Word 2-FrequencyCount");

		// Setting the mapper and the reducer classes. They must extends ContinuousMapper and ContinuousReducer for a continuous job.
		// REQUIRED FOR HADOOP
		job.setMapperClass(Word2FrequencyCountMapper.class);
		job.setReducerClass(Word2FrequencyCountReducer.class);

		// For a continuous job, you HAVE to specify the type of the output of the map
		// REQUIRED FOR CONTINUOUS HADOOP
		job.setMapOutputKeyClass(Text.class) ;
		job.setMapOutputValueClass(IntWritable.class) ;

		// Specifying the type of the output of the job
		// REQUIRED FOR HADOOP
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		// Setting the carry folder (where the carry file will be put if you want to check it.
		// REQUIRED FOR CONTINUOUS HADOOP
		job.setCarryOnFolder(this.carry.toString());


		// Setting the name of the carry files
		// NOT REQUIRED (CONTINUOUS HADOOP)
		job.setCarryFilesName("moncarry") ;

		// Setting the separator that will be used to separate keys and values in the carry files
		// NOT REQUIRED (CONTINUOUS HADOOP)
		job.setCarrySeparator("\u007c") ;


		// Setting the format to read the data in the carry file. It must fit with the KEYIN and VALUEIN of the reducer.
		// NOT REQUIRED (CONTINUOUS HADOOP)
		job.setCarryFormat(TextNumericCarryFormat.class) ;


		// Adding the files contained in the input folder as the input for this job
		// REQUIRED FOR HADOOP
		FileInputFormat.addInputPath(job, this.input);

		// Setting output folder
		// REQUIRED FOR HADOOP
		FileOutputFormat.setOutputPath(job, this.output);

		// Setting Jar file
		// REQUIRED FOR HADOOP
		job.setJarByClass(Word2FrequencyCount.class);

		// Setting the customized parameter for the map reduce job
		// NOT REQUIRED (HADOOP)
		job.setNumReduceTasks(2) ;
		job.getConfiguration().setInt("mapred.min.split.size", 2) ;
		job.getConfiguration().setInt("mapred.map.tasks", 2) ;
		job.getConfiguration().setInt("mapred.tasktracker.map.tasks.maximum", 2) ;

		logger.info("- PRAD - INRIA - : Submitting the job.") ;

		// Submit the job - Blocking
		// REQUIRED FOR HADOOP
		job.waitForCompletion(true);

		logger.info("- PRAD - INRIA - : The job has completed.") ;
	}


	/**
	 * Launches the job.
	 * @param args
	 */
	public static void main (String[] args) {

		if (args.length != 3) {

			logger.error("Expect three arguments : folder input path on hdfs + folder output path on hdfs + folder carry path on hdfs. Warning : input folder must exist already and output folder must not exist already.") ;
			System.out.println("Expect three arguments : folder input path on hdfs + folder output path on hdfs + folder carry path on hdfs. Warning : input folder must exist already and output folder must not exist already.") ;
			System.exit(0) ;
		}

		Word2FrequencyCount wc = new Word2FrequencyCount(new Path (args[0]), new Path(args[1]), new Path(args[2])) ;

		try {

			wc.run() ;
		}

		catch (IOException e) {

			e.printStackTrace();
		}

		catch (InterruptedException e) {

			e.printStackTrace();
		}

		catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
	}
}
