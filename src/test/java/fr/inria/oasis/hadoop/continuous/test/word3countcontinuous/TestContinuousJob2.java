package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * Test class to copy & read a single file inside HDFS using eon cluster. Then
 *
 * it writes a second file. It is using a continuous job + carry with JUnit 4
 *
 * tests.
 *
 *
 *
 * @author adamiano
 *
 *
 */

public class TestContinuousJob2 {
	// The host where to run the jobs
	private static String host;

	// The log
	static private Log LOG = LogFactory.getLog(TestContinuousJob2.class);

	// Test text files
	public static String pathToTextFiles ;
	public static String textFileName;
	public static String textFileName2;

	// Conf
	private static Configuration conf;

	// FS
	private static FileSystem fs;


	// Paths
	private static String ROOT_DIR;
	private static String outputFolder;

	@Before
	public void setUp() throws Exception {
		//Load the properties
		Properties prop = new Properties();

		FileInputStream pathToProperties =  new FileInputStream("./src/fr/inria/oasis/hadoop/continuous/test/word3countcontinuous/test.properties");
		prop.load(pathToProperties);
		pathToProperties.close();

		TestContinuousJob2.host =  prop.getProperty("host");
		TestContinuousJob2.ROOT_DIR = prop.getProperty("ROOT_DIR");
		TestContinuousJob2.pathToTextFiles=prop.getProperty("pathToTextFiles");
		TestContinuousJob2.textFileName=prop.getProperty("textFileName");
		TestContinuousJob2.textFileName2=prop.getProperty("textFileName2");
		TestContinuousJob2.outputFolder = TestContinuousJob2.ROOT_DIR + prop.getProperty("outputFolder");
		//Create the configuration
		conf = new Configuration();
		// Set the config to run on other machine/cluster
		conf.set("fs.default.name", "hdfs://" + host + ":9000");
		conf.set("mapred.job.tracker", host + ":9001");
		conf.set("mapred.continuous.manager.control.host", host);

		// Set the test jar
		conf.set("mapred.jar", "./build/classes/Word3countContinuous.jar");


		// Used to read/write files on HDFS.
		fs = FileSystem.get(conf);
	}

	@Test
	public void TestReadOutputFile1() throws ClassNotFoundException, IOException, InterruptedException {

		// Wait for completion of the new submitted job.
		System.out.println("sleep - wait for the new job to be submitted");

		try {
			TestContinuousJob1.job.waitForResubmitionCompletion();

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		// READ the OUTPUT file
		try {
			// Read the output file
			LOG.info("- PRAD - INRIA - : Reading output for the first test file.");
			Boolean hasReadTheFile = false;

			// For each "continuous_xxx" folder inside continuous-output folder
			FileStatus[] folderStatus = fs.listStatus(new Path(outputFolder));
			if (folderStatus != null) {
				ArrayList<Long> listOfDates = new ArrayList<Long>();
				// For each "continuous_xxx" folder inside output folder
				for (int i = 0; i < folderStatus.length; i++) {
					if (folderStatus[i].getPath().toString().contains("continuous-output")) {
						System.out.println("Found a continuous-output folder");

						// hdfs://eon3.inria.fr:9000/user/adamiano/tmp/test-output/continuous-output_20120521145300
						//Add the date of the folder to the list
						if (folderStatus != null) {
							listOfDates.add(folderStatus[i].getModificationTime());
						}

						//Sort the list
						Collections.sort(listOfDates);
					}
				}
				//read all the folder on the output dir
				FileStatus[] statusFolderbis = fs.listStatus(new Path(outputFolder));

				//But only read inside the most recently modified (last job result)
				for (int k = 0; k < statusFolderbis.length; k++) {
					if(statusFolderbis[k].getModificationTime() == (long)listOfDates.get(listOfDates.size()-1)){
						System.out.println("Found the last continuous-output folder");

						FileStatus[] status = fs.listStatus(statusFolderbis[k].getPath());
						//For each part-r-xxx
						for (int j = 0; j < status.length; j++) {
							if (status[j].getPath().toString().contains("part")) {

								// Open the file
								BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(status[j].getPath())));
								String readedLine = br.readLine();
								int nbLineReaded = 0;

								while (readedLine != null) {
									hasReadTheFile = true;
									nbLineReaded++;
									LOG.info("- PRAD - INRIA - READ : " + readedLine);

									switch (nbLineReaded) {
									case 1:
										assertEquals("tutu\t3", readedLine);
										break;

									default:
										break;

									}
									readedLine = br.readLine();
								}
								// Close the file
								br.close();
							}
						}
					}
				}
			}

			else {
				fail("Error creating or reading the first output file on HDFS");
			}

			if (hasReadTheFile != true) {
				fail("Error creating or reading the first output file on HDFS");
			}
		}

		catch (Exception e) {
			LOG.info("#### Exception in Main");
			e.printStackTrace();
			fail("Error creating or reading the first output file on HDFS");
		}
	}
}
