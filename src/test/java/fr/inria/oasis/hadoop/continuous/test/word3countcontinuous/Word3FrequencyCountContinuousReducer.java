package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import fr.inria.oasis.hadoop.continuous.mapreduce.ContinuousReducer;

public class Word3FrequencyCountContinuousReducer extends ContinuousReducer<Text,IntWritable,Text,IntWritable> {

	protected static Logger logger = Logger.getLogger(Word3FrequencyCountContinuousReducer.class) ;

	@SuppressWarnings("unchecked")
	@Override
	protected void continuousReduce(Text key, Iterable<IntWritable> values,
			ContinuousContext context
			) throws IOException, InterruptedException {

		logger.info("%%%%%%%%%% THE CONTINUOUSREDUCE IS GOING TO START %%%%%%%%%%");
		int sum = 0;
		for(IntWritable i : values) {
			sum = sum+i.get();
		}

		if(sum < 5000) {
			context.carry(key, new IntWritable(sum));
		}

		else {
			context.write(key, new IntWritable(sum));
		}

	}


}
