package fr.inria.oasis.hadoop.continuous.test.word3countcontinuous;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.log4j.Logger;

import fr.inria.oasis.hadoop.continuous.job.ContinuousJob;

public class Word3FrequencyCount {

	protected static Logger logger = Logger.getLogger(Word3FrequencyCountMapper.class) ;

	private Path input ;
	private Path output;
	private Path carry ;


	public Word3FrequencyCount(Path input, Path output, Path carry){
		this.input = input;
		this.output = output;
		this.carry = carry ;
		System.out.println("INPUT PATH :" + this.input + ", OUTPUT PATH : " + this.output + ", CARRY PATH : " + this.carry) ;
	}


	public void run() throws IOException, InterruptedException, ClassNotFoundException{
		ContinuousJob job = new ContinuousJob() ;

		job.setJobName("Word 3-Frequency Count");

		TextOutputFormat.setOutputPath(job, this.output);

		job.setMapperClass(Word3FrequencyCountMapper.class);
		job.setReducerClass(Word3FrequencyCountContinuousReducer.class);

		// input for mapper
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);


		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		//for carry on
		job.setCarryOnFolder(this.carry.toString());

		// add input folder as the input for this job
		FileInputFormat.addInputPath(job, this.input);

		// set output folder
		FileOutputFormat.setOutputPath(job, this.output);

		// set Jar
		job.setJarByClass(Word3FrequencyCount.class);

		logger.info("- PRAD - INRIA - : Submitting the job.") ;

		job.waitForCompletion(true);

		logger.info("- PRAD - INRIA - : The job has completed.") ;
	}


	public static void main (String[] args) {

		if (args.length != 3) {

			logger.error("Expect three arguments : folder input path on hdfs + folder output path on hdfs + folder carry path on hdfs. Warning : input folder must exist already and output folder must not exist already.") ;
			System.out.println("Expect three arguments : folder input path on hdfs + folder output path on hdfs + folder carry path on hdfs. Warning : input folder must exist already and output folder must not exist already.") ;
			System.exit(0) ;
		}

		Word3FrequencyCount wc = new Word3FrequencyCount(new Path (args[0]), new Path(args[1]), new Path(args[2])) ;

		try {

			wc.run() ;
		}

		catch (IOException e) {

			e.printStackTrace();
		}

		catch (InterruptedException e) {

			e.printStackTrace();
		}

		catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
	}

}
