package fr.inria.oasis.hadoop.continuous.test.word2countcontinuous;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import fr.inria.oasis.hadoop.continuous.mapreduce.ContinuousMapper;


/**
 * Represents the mapper class for the Word2FrequencyCount job.
 * Since the job is continuous, the mapper class must extends ContinuousMapper.
 *
 * @author Justine Rochas, Ludwig Poggi, Anthony Damiano, Clement Agarini
 */
public class Word2FrequencyCountMapper extends ContinuousMapper<Text, Text, Text, IntWritable>{

	protected static Logger logger = Logger.getLogger(Word2FrequencyCountMapper.class) ;

	private final static IntWritable one = new IntWritable(1);
	private Text word = new Text();


	/**
	 * Determines what to do with each key and value read in the input files.
	 */
	@Override
	public void map(Text key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString());

		logger.info(" - PRAD - INRIA - : Parsing input") ;

		while (itr.hasMoreTokens()) {

			// Saying that the word read is the key
			word.set(itr.nextToken());

			logger.info(" - PRAD - INRIA - : Generating sample [key=" + word + " value=" + one + "]") ;

			// Sending the computed key and value to the reducer
			context.write(word, one);
		}
	}
}
