#!/bin/bash
#
# This script provides a simple way to install/reinstall hadoop on eon's cluster.
#
# Date : Wednesday 18th April 2012
#

#-- PRODUCTION CONF -- 
# /!\ IF YOU CHANGE THE MASTER NODE DO NOT FORGET TO SCP the scrip deploy.sh to him 


builddir=$1
masternode=$2
hadoopdirectory=$3
targzipname="newinstall"
scripttolaunchonmaster=$5
scripttolaunchonmasterpath=$6
hadoopuser=$7

#-- LOCAL SCRIPT TEST CONF --
#masternode="maledie"
#hadoopuser="cagarini"
#hadoopdirectory="/tmp/testclem"
#scripttolaunchonmaster="propageinstall_local.sh"

if [ -d "$1" ];
	then 
		workingdir="$PWD/"
		echo "-- TAR/GZIP Operation in process --"
		cd $builddir && tar -zcvf $workingdir$targzipname.tar.gz $4 && cd $workingdir
		echo "-- Copying to $masternode --"
		scp $targzipname.tar.gz $hadoopuser@$masternode:$hadoopdirectory
		scp $scripttolaunchonmasterpath $hadoopuser@$masternode:$hadoopdirectory
		echo "-- Lauching script on $masternode to propage installation --"
		ssh $hadoopuser@$masternode chmod +x $hadoopdirectory/$scripttolaunchonmaster
		ssh $hadoopuser@$masternode $hadoopdirectory/$scripttolaunchonmaster $targzipname.tar.gz $hadoopdirectory
		echo "-- Deleting tar/gzip locally --"
		rm $targzipname.tar.gz
	else echo "The specified builddir does not exists"
fi


machine1="tche"
machine2="schubby"
machine3="maledie"
machine4="hajjoura"
