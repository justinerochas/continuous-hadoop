#!/bin/bash
#
#
# This script should be placed on the master of the cluster, he is call after the master has received
# the hadoop install files to propage.
#
# Date : Wednesday, April the 18th
#
# $1 = tar gzip name
# $2 = path
# $3 = chmod dir


if [ -d "$2" ];
then
	cd $2
	cmdtolaunch="cd $2; pwd; tar zxvf $1; rm $1; chmod +x $3 > err.txt; "
	echo "-- Propaging and uncompressing on each node --"

	for i in {1..10}
	do
		if [ "$i" != 2 ] && [ "$i" != 3 ] ;
		then
			echo "eon$i work in progress"s
			scp $1 eon$i:$2
			ssh eon$i $cmdtolaunch     
		fi
     	done
	#after for make it locally
	echo "eon3 work in progress"
        tar zxvf $1
	rm $1
	chmod +x $3
	echo "-- Everything done --"
else echo "The second argument isn't a folder, we need a folder."
fi
	
