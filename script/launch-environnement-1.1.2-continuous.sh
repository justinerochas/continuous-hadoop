# Version $HADOOP_VERSION continuous
# Run this to launch all what is needed to run a continuous job

masternode=$1
HADOOP_VERSION=""

echo "********** Configuring environnement variables **********"
export JAVA_HOME=/local/hadoop/java_latest
export HADOOP_COMMON_HOME=/local/hadoop/hadoop$HADOOP_VERSION-continuous_install
export HADOOP_WORKING_DIR=/local/hadoop/hadoop$HADOOP_VERSION-continuous_workingDirs


echo "********** Stopping all hadoop process **********"
${HADOOP_COMMON_HOME}/bin/stop-cjt.sh
${HADOOP_COMMON_HOME}/bin/stop-mapred.sh
${HADOOP_COMMON_HOME}/bin/stop-dfs.sh
sleep 2


echo "********** Formatting HDFS **********"

for i in {1..10}
	do
		if [ $i !=  2 ];
		then
			echo "--------eon$i------"
			ssh hadoop@eon$i rm -rf /local/hadoop/hadoop$HADOOP_VERSION-continuous_workingDirs/*
		fi
	done


sleep 2
${HADOOP_COMMON_HOME}/bin/hadoop namenode -format


echo "********** Starting Namenode and DataNodes **********"
${HADOOP_COMMON_HOME}/bin/start-dfs.sh

sleep 2
${HADOOP_COMMON_HOME}/bin/hadoop fs -mkdir /local/hadoop
${HADOOP_COMMON_HOME}/bin/hadoop fs -chmod -R a+w /local/hadoop/

${HADOOP_COMMON_HOME}/bin/hadoop fs -mkdir /user/jrochas
${HADOOP_COMMON_HOME}/bin/hadoop fs -chown jrochas:jrochas /user/jrochas

${HADOOP_COMMON_HOME}/bin/hadoop fs -mkdir /user/adamiano
${HADOOP_COMMON_HOME}/bin/hadoop fs -chown adamiano:adamiano /user/adamiano

${HADOOP_COMMON_HOME}/bin/hadoop fs -mkdir /user/cagarini
${HADOOP_COMMON_HOME}/bin/hadoop fs -chown cagarini:cagarini /user/cagarini

${HADOOP_COMMON_HOME}/bin/hadoop fs -mkdir /user/lpoggi
${HADOOP_COMMON_HOME}/bin/hadoop fs -chown lpoggi:lpoggi /user/lpoggi

sleep 2
echo "********** Starting Continuous Job Tracker **********"
${HADOOP_COMMON_HOME}/bin/start-cjt.sh

echo "********** Starting JobTracker and TaskTrackers **********"
${HADOOP_COMMON_HOME}/bin/start-mapred.sh

echo "********** Showing which Java process are running **********"
${JAVA_HOME}/bin/jps

