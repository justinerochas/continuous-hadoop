#!/usr/bin/env bash
#Run this script to install hadoop on a server & it's secondary nodes

HADOOP_VERSION="1.0.2"
SERVERS_NAME="eon"
targzipname="hadoop-continuous-$HADOOP_VERSION"
hadoopuser="hadoop"
masternode="eon3"
hadoopdirectory="/local/hadoop"
distdirectory="../../dist/build/$targzipname"

echo "********** Creating directories **********"
rm -rf ../../../hadoop

mkdir ../../../hadoop
mkdir ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_install
mkdir ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_workingDirs
mkdir ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_config

echo "********** Copying hadoop binaries **********"
cp -r $distdirectory/* ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_install/

echo "********** Copying hadoop configuration **********"
cp  ../continuous_config/* ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_config
#Remove obsolete files inside the new directory
rm -rf ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_install/continuous/continuous_config

echo "********** Copying custom scripts **********"
cp ./start-cjt.sh ../../bin/ 
cp ./stop-cjt.sh ../../bin/ 
cp ./launch-environnement-$HADOOP_VERSION-continuous.sh ../../bin/ 
cp -f ../continuous_config/hadoop-env.sh ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_config 
cp -f ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_config/hadoop-env.sh ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_install/conf

echo "********** Deploying files to servers **********"


for i in {1..10}
	do
		if [ $i !=  2 ];
		then
			echo "--------eon$i------"
			
	


#####
#Create, send & unzip a tar containing all files.
#####
#clean tar
rm -f ./$targzipname.tar
#Create tar
tar -cf  $targzipname.tar ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_install
tar -rf  $targzipname.tar ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_workingDirs
tar -rf  $targzipname.tar ../../../hadoop/hadoop-$HADOOP_VERSION-continuous_config
#make destination directory on the cluster
ssh $hadoopuser@eon$i mkdir $hadoopdirectory
#Copy the tar to the dest dir
scp $targzipname.tar $hadoopuser@eon$i:$hadoopdirectory


#TODO RM temp dir
ssh $hadoopuser@eon$i rm -rf $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_install 
ssh $hadoopuser@eon$i rm -rf $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_config
ssh $hadoopuser@eon$i rm -rf $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_workingDirs




#Extract the tar
ssh $hadoopuser@eon$i "cd $hadoopdirectory; tar xf $targzipname.tar"
ssh $hadoopuser@eon$i "cd $hadoopdirectory;mv $hadoopdirectory/hadoop/hadoop-$HADOOP_VERSION-continuous_install ./hadoop-$HADOOP_VERSION-continuous_install"
ssh $hadoopuser@eon$i "cd $hadoopdirectory;mv $hadoopdirectory/hadoop/hadoop-$HADOOP_VERSION-continuous_workingDirs ./hadoop-$HADOOP_VERSION-continuous_workingDirs"
ssh $hadoopuser@eon$i "cd $hadoopdirectory;mv $hadoopdirectory/hadoop/hadoop-$HADOOP_VERSION-continuous_config ./hadoop-$HADOOP_VERSION-continuous_config"

#Clean tar on the cluster
ssh $hadoopuser@eon$i "cd $hadoopdirectory; rm ./$targzipname.tar"

##Chmod on the scripts...
ssh $hadoopuser@eon$i chmod +x $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_install/bin/*
ssh $hadoopuser@eon$i chmod +x $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_install/conf/*.sh
ssh $hadoopuser@eon$i chmod +x $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_config/*.sh


	fi
done

#Clean tar locally
rm ./$targzipname.tar
#Clean temp dir
rm -rf ../../../hadoop

echo "**** Deploying done ****"

#Copy java
#scp -r ../jdk1.7.0_03 $hadoopuser@$masternode:/$hadoopdirectory

#Make ln -s
#ln -s  ./hadoop-$HADOOP_VERSION-continuous_install/ hadoop
#ln -s ./jdk ... java_latest
#ln -s ./jdk/bin/java java



echo "Would you like to 'start hadoop server' [y/n]?"

read ans

if [ $ans = y -o $ans = Y -o $ans = yes -o $ans = Yes -o $ans = YES ]
then
echo "Starting hadoop server"
ssh $hadoopuser@$masternode $hadoopdirectory/hadoop-$HADOOP_VERSION-continuous_install/bin/launch-environnement-$HADOOP_VERSION-continuous.sh $masternode
fi

