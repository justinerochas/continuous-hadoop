\section{Le modèle MapReduce}
Le modèle MapReduce \cite{mapreduce} est un modèle de programmation parallèle pour
traiter et produire de très grandes quantité de données, de l'ordre du
Teraoctet, avec de bonnes performances. Ce modèle a été présenté par Google dans
une publication de recherches en 2004 \cite{mapreduce_publication}.

\subsection{Fonctionnement}
Ce modèle repose sur une représentation des données dans le format \emph{(clef,
valeur)} et sur l'appel consécutif de deux fonctions : la fonction \emph{Map} et
la fonction \emph{Reduce}. \newline

Initialement, les données se trouvent sous la forme de plusieurs
couples \emph{(clef, valeur)}.
A chacun de ces couples est ensuite appliqué la fonction \emph{Map}.
Cette fonction produit potentiellement un ou plusieurs autres couples \emph{(clef, valeur)} intermédiaires
dérivé du couple \emph{(clef, valeur)} initial. Cette dérivation se fait suivant ce que
l'utilisateur spécifie dans la fonction \emph{Map}. Par exemple, dans le cas du Word 3 Count, voici l'algorithme général appliqué dans la fonction \emph{Map}.

\begin{center}
\begin{minipage}{0.8\textwidth}
\lstset{%
	caption=Descriptive Caption Text,
	basicstyle=\ttfamily\footnotesize\bfseries,
	frame=tb
}
\begin{lstlisting} [caption=Fonction \emph{Map} pour le Word 3 Count, numbers=left]
// key is the line number in the input file
// value is the line content in the input file
map (String key, String value) :
	for each word w in value :
		outputIntermediate(w, 1)
\end{lstlisting}
\end{minipage}
\end{center}

Ces couples intermédiaires sont ensuite rassemblés selon une règle bien
précise : toutes les valeurs de même clef
sont ajoutées dans une collection, produisant ainsi un nouveau couple \emph{(clef,
ensemble de valeurs)}. En même temps, cette phase fait un tri croissant sur les clefs selon leur type. Cette phase s'appelle la phase du \emph{Shuffle/Sort}. \newline

Ensuite, chaque nouveau couple \emph{(clef,
ensemble de valeurs)} est donné en entrée de la fonction \emph{Reduce}. Pour chacun de ces couples, la fonction \emph{Reduce}
traite la clef et l'ensemble de valeurs de la façon dont l'utilisateur
l'a spécifié, pour produire potentiellement un ou plusieurs couples finaux
\emph{(clef, valeur)}. 

\begin{center}
\begin{minipage}{0.8\textwidth}
\lstset{%
	caption=Descriptive Caption Text,
	basicstyle=\ttfamily\footnotesize\bfseries,
	frame=tb
}
\begin{lstlisting} [caption=Fonction \emph{Reduce} pour le Word 3 Count, numbers=left]
// key is the word
// values is the list of occurrences
reduce (String key, Iterator values) :
	int result = 0 
	for each value v in values :
		result += v
	output(key, result)
\end{lstlisting}
\end{minipage}
\end{center}

L'ensemble de ces couples finaux forment la sortie calculée définitive. Pour résumer, voici un aperçu général d'un processus MapReduce.\newline

\begin{figure}[h]
    \center
    \includegraphics[width=15cm]{mapred1.png}
    \caption{Déroulement d'un processus MapReduce}
\end{figure}


\subsection{Distribution}
Ce modèle est particulièrement adapté au calcul réparti. En effet, chaque
fonction \emph{Map} peut s'exécuter en parallèle, puisque ces fonctions sont toutes
indépendantes les unes des autres. De plus, elles exécutent toutes le même code, ce qui permet de bien équilibrer la charge de travail entre toutes les ressources disponibles. Ces fonctions \emph{Map} peuvent donc
aisément s'exécuter sur plusieurs unités de calcul différentes, par
exemple à travers un cluster.\newline

D'autre part, bien que la fonction \emph{Reduce} semble non-parallélisable, il est possible
d'exécuter plusieurs de ces fonctions en même temps pour le même résultat : chaque fonction
\emph{Reduce} peut prendre une partie des clefs à réduire et la traiter indépendamment des autres parties. L'autre avantage est que
cette spécification en une suite d'étapes permet aux implémentations du modèle 
MapReduce d'encapsuler les étapes de distribution et de regroupement, ce qui
facilite l'écriture d'applications distribuées et les rend plus robustes.

\section{Hadoop}
\subsection{Présentation}
Hadoop \cite{hadoop} est un framework issue de la fondation Apache qui permet le traitement
distribué intensif de très grands ensembles de données.
C'est une des implémentations du modèle MapReduce dans le langage
Java. Entre autres utilitaires, Hadoop contient deux composants
principaux qui permettent de réaliser les objectifs du modèle MapReduce.\newline
D'une part, Hadoop offre une API qui permet d'écrire et de paramétrer des jobs. Cette API permet de spécifier le comportement des
fonctions \emph{Map} et \emph{Reduce} à appliquer aux données spécifiées
en entrée. 

Généralement, l'utilisateur crée trois classes :
\begin{itemize}
\item La première étend la classe \emph{Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT>}.
Les quatre paramètres représentent les type en entrée et en sortie du \emph{Map} pour la clef et la valeur. On peut dans cette classe redéfinir la méthode \emph{map(KEYIN key, VALUEIN value, Context context)} pour préciser ce que Hadoop doit appliquer sur les clefs et les valeurs lues.
\item La deuxième étend la classe \emph{Reducer<KEYIN, VALUEIN, KEYOUT, VALUEOUT>}.
Les quatre paramètres ont la même sémantique que précédemment mais pour l'entrée et la sortie du \emph{Reduce}. On peut dans cette classe redéfinir la méthode \emph{reduce(KEYIN key, Iterable<VALUEIN> values, Context context)} pour préciser ce que Hadoop doit appliquer sur les clefs et les ensembles de valeurs créés par l'étape \emph{Map}.
\item La troisième crée une instance de \emph{Job}, le paramètre à travers un objet \emph{JobConf} qui lui est lié, et le soumet au cluster par la méthode \emph{waitForCompletion()}. \newline
\end{itemize}


D'autre part, il offre un système de fichiers distribué dédié, nommé HDFS, Hadoop Distributed File System, qui est une implémentation open source du Distributed Google File System \cite{google_fs}, où sont lus les fichiers contenant les données
d'entrées et où sont produits les fichiers contenant la sortie finale d'un
processus MapReduce. HDFS utilise la réplication des fichiers pour être tolérant aux pannes. Ce système de fichiers est distribué, il permet donc le stockage de fichiers très lourds, puisque un seul et même fichier peut être divisé en plusieurs morceaux pour être stocké équitablement entre les ressources disponibles. Le stockage de gros fichiers est même recommandée puisque HDFS utilise des blocs qui sont par défaut fixés à 64 Mo, donc toute utilisation de fichiers dont la taille est inférieure à 64 Mo représente une perte d’espace disque sur HDFS.\newline

La force de ce framework vient de la possibilité d'utiliser des clusters composés de plusieurs dizaines de machines
sans devoir se préoccuper de leurs communications. Plus précisément, il suffit de
spécifier les machines à prendre en compte à travers plusieurs
fichiers de configuration XML, c'est ensuite le code de Hadoop qui lit ces informations et qui gère la distribution des tâches et des données, et les points de synchronisation. Le déroulement d'un job Hadoop est ainsi
hautement configurable sans avoir de connaissances particulières dans le domaine des communications distantes. 

\subsection{Fonctionnement}

\subsubsection{Serveurs}

Pour qu'un job Hadoop puisse être exécuté, il faut démarrer au préalable des programmes Java sur chacune des machines participant au job. Ce sont des serveurs qui ont chacun un rôle dans le déroulement d'un job. 

Deux types de serveurs existent : 

\begin{itemize}
\item{Les serveurs gérant le système de fichiers} La plupart d'entre eux, les \emph{DataNode}, procurent chacun un espace de stockage. Ces espaces disque sont concaténés pour former un seul espace de stockage logique. Un seul autre serveur, le \emph{NameNode} a pour rôle de maintenir les métadonnées du système de fichiers distribué. C'est l'unique point d'accès au données stockées sur HDFS. Quand un fichier doit être stocké sur HDFS, le \emph{NameNode} distribue ses blocs de 64 Megaoctets qui le compose aux différents \emph{DataNode}. Voici un schéma, extrait du livre \emph{Hadoop : The Definitive Guide} \cite{hadoop_book_1}, qui montre l'accès aux données sur HDFS.

\begin{figure}[!h]
    \center
   \includegraphics[width=13cm]{hdfs.jpg}
   \caption{Gestion de l'accès aux données sur HDFS}
\end{figure}

\item{Les serveurs dédiés au processus MapReduce} Un seul de ces serveurs a le contrôle sur la distribution du job, c'est le \emph{JobTracker}. Les autres serveurs, appelés les \emph{TaskTracker}, reçoivent et exécutent les tâches envoyées par le \emph{JobTracker}.
Concrètement, l'exécution d'un job se déroule comme suit. Le code client crée une instance de \emph{Job} et appelle la méthode \emph{waitForCompletion()} pour soumettre le job au cluster. Plus précisément, le job est soumis au \emph{JobTracker} qui le met dans une queue gérée par un objet \emph{JobClient} qui schedule les jobs. Quand son tour arrive, une liste de tâches est créée et ces tâches sont distribuées aux \emph{TaskTracker}. Quand le job termine, des informations telles que le temps d'exécution, la distribution des tâches ou les erreurs rencontrées sont disponibles à travers l'instance de \emph{Job}. Voici un schéma, toujours extrait du même livre \cite{hadoop_book_1}, qui montre le déroulement d'un job Hadoop.

\begin{figure}[!h]
    \center
    \includegraphics[width=13cm]{job.png}
    \caption{Fonctionnement d'un processus MapReduce avec Hadoop}
\end{figure}

\end{itemize}

Généralement, le \emph{JobTracker} et le \emph{NameNode} s'exécutent sur une même machine physique désignée comme machine maître. Les \emph{TaskTracker} et les \emph{DataNode} s'exécutent eux sur les machines spécifiées comme esclaves. \newpage

\subsubsection{Données}

Les fichiers contenant les données à traiter doivent être ajoutées au préalable sur le système de fichiers de Hadoop. Pour extraire les données des fichiers d'entrée, Hadoop utilise un fonctionnement à deux niveaux. \newline

Le premier niveau de lecture du fichier correspond à l'application un format, appelé \emph{InputFormat}, qui doit être spécifié par l'utilisateur. Plusieurs formats sont disponibles et l'utilisateur peut créer ses propres formats de lecture. Ce format sert à diviser les fichiers d'entrée en plusieurs \emph{InputSplit} qui sont de la taille des blocs sur HDFS (64 Megaoctets par défaut). Chaque \emph{InputSplit} va être traité par une seule machine. \newline

Mais avant de pouvoir appliquer la fonction \emph{Map}, les clefs et les valeurs doivent être extraites de ces \emph{InputSplit}. C'est là que le deuxième niveau de lecture intervient. Un lecteur, appelé \emph{RecordReader}, va permettre de parser les données contenues dans un \emph{InputSplit}. Ce lecteur est déterminé par le format de lecture, l'utilisateur n'a donc rien à spécifier en plus. Ce sont les données extraites par le \emph{RecordReader} qui vont finalement être passées aux fonctions \emph{Map}. \newline
 
A l'issue d'une fonction \emph{Map}, les données intermédiaires sont écrites dans un fichier se trouvant sur le système de fichiers local à la machine hébergeant la fonction \emph{Map}. Le processus qui s'occupe de ramasser ces fichiers lit les données qui s'y trouvent et procède à la phase de \emph{Shuffle/Sort}. Les données triées sont ensuite partitionnées en fonction du nombre de fonctions \emph{Reduce} qui vont s'exécuter, ce nombre étant fixé par l'utilisateur. Ensuite, chaque partition est copiée sur le système de fichiers local à la machine qui va exécuter le \emph{Reduce}.
Enfin les fonctions \emph{Reduce} vont produire leur sortie définitive contenue dans un fichier écrit sur HDFS. Pour se faire, Hadoop utilise là encore des formats, tous dérivés de \emph{OuputFormat}. Comme pour la lecture, il déterminent la façon dont vont être écrites les données dans les fichiers de sortie. Pour effectuer l'écriture symétriquement à la lecture, Hadoop utilise des écrivains, appelés \emph{RecordWriter}. Après l'écriture des sorties, les fichiers produits sont répliqués pour disposer de sauvegardes en cas de panne. \newline

Le schéma suivant, extrait du livre cité précédemment \cite{hadoop_book_1}, récapitule le transit des données à travers un job Hadoop.

\begin{figure}[!h]
    \center
    \includegraphics[width=13cm]{split.png}
    \caption{Transit des données dans un job Hadoop}
\end{figure}


\section{Ressources fournies}
Il y a trois ressources majeures fournies pour ce TER.

\begin{itemize}

\item La première ressource est matérielle. Un cluster de dix machines se trouvant à l'INRIA, à Sophia Antipolis, est mit à disposition pour lancer des jobs Hadoop de façon distribuée. Sur ces machines sont installés une version classique de Hadoop en version 0.21.0 avec les fichiers de configurations propres à l'INRIA afin de lancer des jobs standards sur ces machines. Il est permis à la fois d'utiliser cette installation pour étudier son fonctionnement, et d'installer notre propre version continue et de l'utiliser en conditions réelles, c'est-à-dire avec un nombre de machines important et de grandes quantités de données à traiter. Il est à noter que cela fait partie du travail de savoir déployer la version continue de Hadoop sur ces machines, de savoir la configurer, et de savoir l'utiliser.\newline

\item La deuxième ressource consiste en une explication des concepts sous-jacents à cette extension dans un article de publication \cite{lightweight_mechanism}. Cet article met en évidence les questions soulevées par l'extension continue de Hadoop, comme le choix des nouvelles données à prendre en compte ou encore la position de l'étape de la mise en attente des données, et donne un moyen conceptuel parmi tant d'autres de répondre à ces questions.\newline

\item La troisième ressource, logicielle, est un prototype du travail demandé. Cette implémentation avait été réalisée par un
étudiant, Trong-Tuan Vu, en stage de fin d'études en 2011 et compatible pour Hadoop en version 0.21.0.
En Décembre 2011, Hadoop version 1.0.0 a été lancé et cette mise à jour majeure
a entraîné l'incompatibilité du travail précédent, puisque le code source de Hadoop avait été modifiée pour que le prototype fonctionne. Ce prototype contient à la fois le code ajouté par Trong-Tuan Vu et l'ensemble du code source de Hadoop directement modifié pour l'occasion. Cela représente un projet d'environ 7000 fichiers, dont la moitié contient du code source Hadoop. Le reste des fichiers concernent les scripts de lancement et de configuration d'Hadoop, ainsi que des utilitaires requis pour la compilation du projet. \newline
\end{itemize}

Plus précisément, voici ce qui est réalisé et ce qui est incomplet ou manquant dans le prototype fourni.

\begin{enumerate}
\item Pour gérer la succession de jobs, un serveur appelé le ContinuousJobTracker vient s'ajouter aux autres serveurs de Hadoop. Il sert à re-soumettre les sous jobs mais il ne stocke aucune information sur les sous jobs lancés.

\item Pour ne filtrer que les nouvelles données en entrée, une stratégie d'horodatage s'effectue sur chaque ligne de tous les fichiers texte passés en entrée. Ainsi les données qui ont un horodatage trop vieux (i.e. qui correspondaient à un précédent sous job) ne sont pas chargées pour le nouveau sous job. Pour ce faire, un nouveau format de lecture est introduit : le \emph{ContinuousInputFormat}. Cette stratégie fait grossir inutilement la taille des fichiers d'entrée et prend plusieurs minutes à s'effectuer pour des fichiers de quelques Gigaoctets.

\item La gestion des données mises en attente à la fin d'un sous job se fait directement dans le code client. L'utilisateur ouvre un fichier sur HDFS et écrit des données qu'il souhaite mettre en attente dedans. Il aussi est à sa charge de faire en sorte que les données soient rechargées et lues au bon format pour le prochain sous job. Cette étape manque cruellement d'une API qui prendrait en charge les aspects techniques de la mise en attente des données.

\item Dans le but de  déclencher un nouveau sous job quand des données sont copiées sur HDFS, le serveur appelé NameNode est directement modifié dans Hadoop. Par réactions en chaîne, 12 classes Hadoop sont modifiées pour prendre en charge ces extensions.\newline
\end{enumerate}

Le prototype fourni montre qu'une notion de continuité peut être implémentée en utilisant Hadoop, mais aucune spécification précise d'un job continu est donnée. Il fonctionne uniquement pour l'usage qui en a été fait. En effet, le cas d'utilisation développé précédemment concernait l'exécution de requêtes en chaîne pour la mise à jour de ressources RDF \cite{rdf} , et le code de ce cas d'utilisation était mêlé à l'implémentation continue de Hadoop. L'implémentation résultante de Hadoop continu manque de spécifications qui rendraient ce modèle pertinent pour tout un ensemble de jobs. Elle manque aussi de tests qui permettraient de confirmer la viabilité de l'extension en validant chaque étape du déroulement d'un job continu. Enfin, elle est trop liée à une version précise de Hadoop, ce qui la rend complètement statique.
